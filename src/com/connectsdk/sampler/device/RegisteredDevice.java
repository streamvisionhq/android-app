package com.connectsdk.sampler.device;

/**
 * Created by pc on 6/23/2015.
 */
public class RegisteredDevice {


    private String deviceId;
    private String accountId;
    private String identifier;
    private String serviceType;
    public boolean isSelected;
    public boolean online;


   public RegisteredDevice(String deviceId, String accountId, String identifier, String serviceType){
       this.deviceId = deviceId;
       this.accountId = accountId;
       this.identifier = identifier;
       this.serviceType = serviceType;

   }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
