package com.connectsdk.sampler.widget;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectsdk.device.ConnectableDevice;
import com.connectsdk.sampler.R;
import com.connectsdk.sampler.device.RegisteredDevice;
import com.connectsdk.service.DeviceService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SettingsListAdapter extends BaseAdapter {

    List<ConnectableDevice> myList = new ArrayList<ConnectableDevice>();
    LayoutInflater inflater;
    Context context;
    List<ConnectableDevice> myList2 = new ArrayList<ConnectableDevice>();
    List<ConnectableDevice> toShowList = new ArrayList<ConnectableDevice>();
    ArrayList<RegisteredDevice> registeredList = new ArrayList<RegisteredDevice>();
 
 
    public SettingsListAdapter(Context context, List<ConnectableDevice> _myList, ArrayList<RegisteredDevice> _registeredList) {
        this.myList = _myList;

        this.context = context;
        inflater = LayoutInflater.from(this.context);
        registeredList = _registeredList;

        for (int i=0; i< myList.size();i++) {
            //for(int j=0; i<myList.size();j++) {
                //if (registeredList.get(i).
            //}
            myList.get(i).getId();
            Iterator itr = myList.get(i).getServices().iterator();
            while(itr.hasNext()) {
                DeviceService s = (DeviceService)itr.next();

                Log.d("TEST_ROKU_3", "  " + s.getServiceConfig().getServiceUUID());
                if(s.getServiceConfig().getServiceUUID().indexOf("roku:ecp:") > -1) {
                    myList.get(i).setModelNumber(
                            s.getServiceConfig().getServiceUUID().substring(s.getServiceConfig().getServiceUUID().indexOf("roku:ecp:") + 9));
                    break;
                }
                //if(registeredList)
            }
            String s = myList.get(i).getModelNumber();

            boolean found = false;
            for (int j=0;j<registeredList.size();j++) {
                Log.d("TEST_ROKU_3", registeredList.get(j) + " -- " + s);

                if(registeredList.get(j).getDeviceId().equals(s))
                {
                    found = true;
                    Log.d ("TEST_ROKU_3", s + " to test this string in server found " + s);
                    //myList2.add(myList.get(i));
                    registeredList.get(j).online = true;

                }
            }


         //   Log.d("TEST_ROKU_3", );
        }
        notifyDataSetChanged();
    }

    public void updateData(List<ConnectableDevice> _myList, ArrayList<RegisteredDevice> _registeredList) {
        this.myList = _myList;

        registeredList = _registeredList;
        for (int j=0;j<registeredList.size();j++) {
            registeredList.get(j).online = false;
        }
        for (int i=0; i< myList.size();i++) {
            //for(int j=0; i<myList.size();j++) {
            //if (registeredList.get(i).
            //}
            myList.get(i).getId();
            Iterator itr = myList.get(i).getServices().iterator();
            while(itr.hasNext()) {
                DeviceService s = (DeviceService)itr.next();

                Log.d("TEST_ROKU_7", "  " +s.getServiceConfig().getServiceUUID());

                if(s.getServiceConfig().getServiceUUID().indexOf("roku:ecp:") > -1) {
                    myList.get(i).setModelNumber(
                            s.getServiceConfig().getServiceUUID().substring(s.getServiceConfig().getServiceUUID().indexOf("roku:ecp:") + 9));
                    break;
                }
                //if(registeredList)
            }
            String s = myList.get(i).getModelNumber();

            boolean found = false;
            for (int j=0;j<registeredList.size();j++) {
                //Log.d("TEST_ROKU_3", registeredList.get(j) + " -- " + s);
                if(registeredList.get(j).getDeviceId().equals(s))
                {
                    found = true;
                    Log.d ("TEST_ROKU_3", s + " to test this string in server found " + s);
                    //myList2.add(myList.get(i));
                    registeredList.get(j).online = true;

                }

            }


            //   Log.d("TEST_ROKU_3", );
        }

        notifyDataSetChanged();
    }
 
    @Override
    public int getCount() {
        return registeredList.size();
    }
 
    @Override
    public RegisteredDevice getItem(int position) {
        RegisteredDevice s = registeredList.get(position);
        return s;
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;
 
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_devicelist_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        RegisteredDevice currentListData = getItem(position);
 
        mViewHolder.devieId.setText(currentListData.getDeviceId());
        /*String toCompare = currentListData.getModelNumber();
        if(registeredList.indexOf(toCompare)>-1){
            mViewHolder.device_Register.setText(" ");
        }
        else{
            //return null;
            mViewHolder.device_Register.setText(" ");
        }*/
        mViewHolder.device_Register.setText(currentListData.getAccountId());
        mViewHolder.device_identifier.setText(currentListData.getIdentifier());
        mViewHolder.serviceType.setText(currentListData.getServiceType());
        if(currentListData.online) mViewHolder.online.setImageResource(R.drawable.online);
        else mViewHolder.online.setImageResource(R.drawable.offline);
        mViewHolder.select.setChecked(currentListData.isSelected);

//        mViewHolder.select.setSelected();
 
        return convertView;
    }
 
    private class MyViewHolder {
        TextView devieId;
        TextView device_Register;
        TextView device_identifier;
        TextView serviceType;
        ImageView online;
        CheckBox select;

        public MyViewHolder(View item) {
            devieId = (TextView) item.findViewById(R.id.deviceIdtext);
            device_Register = (TextView) item.findViewById(R.id.registertextId);
            device_identifier = (TextView) item.findViewById(R.id.deviceIdentifierText);
            serviceType = (TextView) item.findViewById(R.id.serviceTypeId);
            online = (ImageView) item.findViewById(R.id.deviceOnlineText);
            select = (CheckBox) item.findViewById(R.id.deviceCheckboxSelectId);
        }
    }
}