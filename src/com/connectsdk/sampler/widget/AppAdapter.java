//
//  Connect SDK Sample App by LG Electronics
//
//  To the extent possible under law, the person who associated CC0 with
//  this sample app has waived all copyright and related or neighboring rights
//  to the sample app.
//
//  You should have received a copy of the CC0 legalcode along with this
//  work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
//

package com.connectsdk.sampler.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.connectsdk.core.AppInfo;
import com.connectsdk.sampler.R;
import com.connectsdk.sampler.util.ImageLoader;

import org.apache.http.HttpStatus;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Comparator;

public class AppAdapter extends ArrayAdapter<AppInfo> {
    Context context; 
    int resourceId; 
    String runningAppId;
    ImageView imageView;
    private ProgressBar progressBar;
    private String appId;
    ImageLoader imageLoader;
    public AppAdapter(Context context, int resource,String appId) {
        super(context, resource);
        this.context = context;
        this.resourceId = resource;
        this.appId = appId;
         imageLoader = new ImageLoader(context);
        Log.d("Application List ","ffffffffffffffffffffff");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Log.d("Application List ","eeeeeeeeeeeeeeee");
        if (convertView == null) {
            view = View.inflate(getContext(), resourceId, null);
        }else{
            view = (View) convertView;
        }

        AppInfo app = this.getItem(position);
//        TextView textView = (TextView) view.findViewById(R.id.channelName);
//        TextView subTextView = (TextView) view.findViewById(R.id.itemSubTitle);
        imageView = (ImageView) view.findViewById(R.id.icon);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
//        textView.setText(app.getName());
//        subTextView.setText(app.getId());
//            textView.setTextColor(Color.WHITE);
        if(imageView != null){
//            new ImageDownloaderTask(imageView,progressBar,this ).execute(appId+app.getId());
            imageLoader.DisplayImage(appId+app.getId(),imageView);

        }

        return view;
    }

    public void setRunningAppId(String appId) {
        runningAppId = appId;
    }

    public void sort() {
        this.sort(new Comparator<AppInfo>() {
            @Override
            public int compare(AppInfo lhs, AppInfo rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
    }


    class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;

        private ProgressBar progressbar;
        AppAdapter adapter;

        public ImageDownloaderTask(ImageView imageView, ProgressBar progressbar, AppAdapter adapter) {
            imageViewReference = new WeakReference<ImageView>(imageView);
            this.progressbar = progressbar;
            this.adapter = adapter;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadBitmap(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null && bitmap != null) {
                ImageView imageView = imageViewReference.get();
                if (imageView != null) {

                        imageView.setImageBitmap(bitmap);
                        progressbar.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
//                    else {
//                        Drawable placeholder = imageView.getContext().getResources().getDrawable(R.drawable.netflix);
//                        imageView.setImageDrawable(placeholder);
//                    }
                }
            }
        }
    }


    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode != HttpStatus.SC_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        } catch (Exception e) {
            urlConnection.disconnect();
            Log.w("ImageDownloader", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

}
