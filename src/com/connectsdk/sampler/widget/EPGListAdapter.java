package com.connectsdk.sampler.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectsdk.sampler.R;
import com.connectsdk.sampler.epg.Channel;

import org.apache.http.HttpStatus;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class EPGListAdapter extends BaseAdapter {
    private Activity mContext;
    private ArrayList<Channel> channels;
    private LayoutInflater mLayoutInflater = null;
    public Bitmap[] downloadedBitmaps = new Bitmap[100];
    CompleteListViewHolder viewHolder;
    public EPGListAdapter(Activity context, ArrayList<Channel> channels) {
        mContext = context;
        this.channels = channels;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return channels.size();
    }

    @Override
    public Object getItem(int pos) {
        return channels.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.epglist_item, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
            Channel channel = channels.get(position);
            if(channel.getDescription().equals(""))
                viewHolder.mTVItem.setText(channel.getChannelId());
            else viewHolder.mTVItem.setText(channel.getDescription());
            if(viewHolder.icon != null){
                new ImageDownloaderTask((ImageView)v.findViewById(R.id.epgicon), position).execute(channel.getChannelLogo());
            }
        } else {
            Channel channel = channels.get(position);
            viewHolder = (CompleteListViewHolder) v.getTag();
            if(channel.getDescription().equals(""))
                viewHolder.mTVItem.setText(channel.getChannelId());
            else viewHolder.mTVItem.setText(channel.getDescription());
            if(viewHolder.icon != null){
                new ImageDownloaderTask((ImageView)v.findViewById(R.id.epgicon), position).execute(channel.getChannelLogo());
            }

        }


//        ImageLoader imgLoader = new ImageLoader(mContext);
//
//        Log.d("Application List ",channel.getChannelLogo());
//        imgLoader.DisplayImage(channel.getChannelLogo(), R.drawable.netflix, viewHolder.icon);
//        Log.d("Details-->",channel.getChannelNumber());
        return v;
    }

   public class CompleteListViewHolder {
        public TextView mTVItem;
       public ImageView icon;

        public CompleteListViewHolder(View base) {
            mTVItem = (TextView) base.findViewById(R.id.EpgchannelName);
            icon = (ImageView) base.findViewById(R.id.epgicon);
        }
    }


    class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private final ImageView imageViewReference;
        int positionOfBitmap;

        public ImageDownloaderTask(ImageView imageView, int index) {
            imageViewReference = imageView;
            positionOfBitmap = index;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            if(downloadedBitmaps[positionOfBitmap] == null)
            downloadedBitmaps[positionOfBitmap] = downloadBitmap(params[0]);
            return downloadedBitmaps[positionOfBitmap];
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference;
                if (imageView != null) {
                    if (bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                    } else {
                        Drawable placeholder = imageView.getContext().getResources().getDrawable(R.drawable.default_channel);
                        imageView.setImageDrawable(placeholder);
                    }
                }
            }
        }
    }


    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode != HttpStatus.SC_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        } catch (Exception e) {
            urlConnection.disconnect();
            Log.w("ImageDownloader", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            //Log.d("TEST_ROKU", urldisplay);
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}


