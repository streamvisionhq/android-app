package com.connectsdk.sampler.epg;

import java.util.Date;

/**
 * Created by Vivek on 18/06/15.
 */
public class ProgramData {
    public Date startTime;
    public Date endTime;
    public String Title;
    public String Description;
    public String channelId;
    public String imageURL;
    public String svChannelId;

}
