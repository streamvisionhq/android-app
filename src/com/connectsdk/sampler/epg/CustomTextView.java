package com.connectsdk.sampler.epg;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;


/**
 * An extension of TextView that applies the custom typeface automatically.
 * Please note that the custom typeface will <b>NOT</b> be visible when previewing in XML layouts.
 *
 * @author Eric Lee<eric.lee@accedo.tv>
 * @since VIA App 1.2.0
 */
public class CustomTextView extends TextView {

    public GestureDetector gestureDetector;
    private Context mContext;
    /**
     * A simple constructor for creating from code.
     *
     * @param context The Context the view is running in
     */
    public CustomTextView(final Context context) {
        super(context);
        mContext = context;
        initialize(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if(gestureDetector!=null)
        return gestureDetector.onTouchEvent(e);
        else return true;
    }

    public void setGestureListener(GestureDetector.SimpleOnGestureListener listener) {
        gestureDetector = new GestureDetector(mContext, listener);
    }

    /**
     * Constructor that is called when inflating a view from XML.
     * This is called when a view is being constructed from an XML file, supplying attributes that were specified in the XML file.
     *
     * @param context The Context the view is running in
     * @param attrs The attributes of the XML tag that is inflating the view.
     */
    public CustomTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(final Context context) {
        // This should only be used when not inflating from XML

    }

    @Override
    public void setText(final CharSequence text, final BufferType type) {
        final CharSequence translation;
            translation = text;
        super.setText(translation, type);
    }
}