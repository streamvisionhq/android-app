package com.connectsdk.sampler.epg;

/**
 * Created by Vivek on 18/06/15.
 */
// CHECKSTYLE:OFF

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.connectsdk.sampler.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"JavaDoc", "PMD"})
public class ChannelListAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<Channel> mData = new ArrayList<>();

    private boolean mShowdivider = true;

    public ChannelListAdapter(final Context context, final List<Channel> data) {
        mContext = context;
        mData.addAll(data);
       }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Channel getItem(final int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return 0;
    }

    /**
     * Draw custom view
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        final View view;
        final Channel data = getItem(position);
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.tv_guide_listview_item, parent, false);
            new DownloadImageTask((ImageView) view.findViewById(R.id.tv_guide_channel_logo))
                   .execute(data.getChannelLogo());
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }


        return view;
    }



    private static class ViewHolder {
        private ImageView logo;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            //Log.d("TEST_ROKU", urldisplay);
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            if(result!= null)
            bmImage.setImageBitmap(result);
            else
                bmImage.setImageResource(R.drawable.default_channel);
        }
    }

}