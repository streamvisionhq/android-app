package com.connectsdk.sampler.epg;

/**
 * Created by pc on 6/1/2015.
 */
public class Channel {

    private String channelId;

    private String channelNumber;

    private String description;

    private String channelLogo;

    private String svChannelId;

    public Channel(String channelId, String channelNumber, String description, String channelLogo, String svChannelId){

        this.channelId = channelId;
        this.channelNumber = channelNumber;
        this.channelLogo = channelLogo;
        this.description = description;
        this.svChannelId = svChannelId;
    }


    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(String channelNumber) {
        this.channelNumber = channelNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChannelLogo() {
        return channelLogo;
    }

    public void setChannelLogo(String channelLogo) {
        this.channelLogo = channelLogo;
    }

    public String getSvChannelId() {
        return svChannelId;
    }

    public void setSvChannelId(String svChannelId) {
        this.svChannelId = svChannelId;
    }
}
