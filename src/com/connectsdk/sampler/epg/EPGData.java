package com.connectsdk.sampler.epg;

import java.util.ArrayList;

/**
 * Created by Vivek on 18/06/15.
 */
public class EPGData {
    public ArrayList<ProgramData> programList = new ArrayList<ProgramData>();

    public String channelId;
    public String channelNumber;
    public String svChannelId;
}
