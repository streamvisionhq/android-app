package com.connectsdk.sampler.util;


/**
 * Created by Vivek on 21/07/15.
 */
public interface CallBack<T> {
    void execute(final T response);
}
