package com.connectsdk.sampler.util;

/**
 * Created by Vivek on 24/07/15.
 */

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * This Class Download Data from the api and return response to the callback function
 * Created by Vivek on 20/07/15.
 */
public class HttpUtil extends AsyncTask<Void, Void, ArrayList<Boolean>> {


    ArrayList<String> urls = new ArrayList<String>();
    String auth_header;
    CallBack<ArrayList<Boolean>> resultCallBack;
    //CallBa

    public HttpUtil(ArrayList<String> _urls, CallBack<ArrayList<Boolean>> onResult) {
        this.urls.addAll(_urls);
         resultCallBack = onResult;
    }

    @Override
    protected ArrayList<Boolean> doInBackground(Void... params) {
        ArrayList<Boolean> result = new ArrayList<Boolean>();
        try {


            try {
                try {
                    for(int i=0;i<urls.size();i++) {
                        Log.d("TEST_ROKU_7", "VALIDATION "+ urls.get(i)  );
                        Socket s = new Socket();
                        String host = urls.get(i);

                        try
                        {
                            s.connect(new InetSocketAddress(host , 8060), 1000);
                        }

                        //Host not found
                        catch (UnknownHostException e)
                        {
                            result.add(false);
                            continue;
                        }
                        catch (SocketTimeoutException e)
                        {
                            result.add(false);
                            continue;
                        }
                        result.add(true);
                        s.close();
                       // result.add(InetAddress.getByName(urls.get(i)).isReachable(1000));
                        Log.d("TEST_ROKU_7", "VALIDATION " + urls.get(i) + result.get(i));
                    }
                } catch (IOException e) {
                    Log.d("TEST_ROKU_7", "error " + e.getMessage());
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {


        }
        return result;
    }

    @Override
    protected void onPostExecute(ArrayList<Boolean> result) {
        //return result;
        if(resultCallBack!=null)
            resultCallBack.execute(result);
    }
}

