package com.connectsdk.sampler.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Created by Vivek on 08/07/15.
 */
public class CustomImageView extends ImageView {


    public GestureDetector gestureDetector;
    private Context mContext;

    public CustomImageView(Context context) {
        super(context);
        mContext = context;
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
    }


    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return gestureDetector.onTouchEvent(e);
    }

    public void setGestureListener(GestureDetector.SimpleOnGestureListener listener) {
        gestureDetector = new GestureDetector(mContext, listener);
    }
}
