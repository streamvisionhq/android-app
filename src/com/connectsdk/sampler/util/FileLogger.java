package com.connectsdk.sampler.util;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * Logs strings to a file on the SD card.
 * Created by Grey on 11.02.2016.
 */
public class FileLogger {

    /**
     * Logs a line to streamvision.log in the SD card or internal memory of the device.
     * @param line the line to log. Datetime is prepended automagically.
     */
    public static void Log(String line){
        File sdCard = Environment.getExternalStorageDirectory();
        File file = new File(sdCard, "streamvision.log");

        try {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            Calendar cal = Calendar.getInstance();
            Date now = cal.getTime();

            writer.append(now.toString()+": "+line);
            writer.newLine();

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
