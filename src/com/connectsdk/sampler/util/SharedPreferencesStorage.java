package com.connectsdk.sampler.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.connectsdk.sampler.models.ServiceProvider;
import com.google.gson.Gson;

/**
 * Stores the customer last name and provider ID for later usage.
 */
public class SharedPreferencesStorage {

    public static final String MY_PREFS_NAME = "StreamVision";
    public static final String KEY_CUSTOMERID = "lastName";
    public static final String KEY_PROVIDERID = "providerId";
    public static final String KEY_SERVICEPROVIDER = "serviceProvider";
    public static final String KEY_PASSWORD = "password";

    /**
     * Stores the customer's id.
     * @param context The app context
     * @param customerId
     */
    public static void saveCustomerId(Context context, String customerId){
        store(context, KEY_CUSTOMERID, customerId);
    }

    /**
     * Get the customer id stored here.
     * @param context
     * @return the Saved CustomerId
     */
    public static String getCustomerId(Context context){
        return get(context, KEY_CUSTOMERID);
    }

    /**
     * Stores the entered provider ID.
     * @param context The app context
     * @param providerId
     */
    public static void saveProviderId(Context context, String providerId){
        store(context, KEY_PROVIDERID, providerId);
    }

    /**
     * Get teh provider ID stored here.
     * @param context
     * @return the Saved CustomerId
     */
    public static String getProviderId(Context context){
        return get(context, KEY_PROVIDERID);
    }

    /**
     * It Clear all saved data for the given preference name
     * @param context
     */
    public static void clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME,
                context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Turns a ServiceProvider object to JSON and stores it in shared prefs.
     * @param context
     * @param provider
     */
    public static void saveServiceProvider(Context context, ServiceProvider provider){
        Gson gson = new Gson();
        String json = gson.toJson(provider);
        Log.d("ServiceProvider", "Saving to SP: " + json);
        store(context, KEY_SERVICEPROVIDER, json);
    }

    /**
     * Gets the service provider stored as JSON, builds the object and returns it.
     * @param context
     * @return
     */
    public static ServiceProvider getServiceProvider(Context context){
        String json = get(context, KEY_SERVICEPROVIDER);
        Log.d("SharedPrefs", "Getting from SP: " + json);
        Gson gson = new Gson();
        return gson.fromJson(json, ServiceProvider.class);
    }

    /**
     * Save the password the user entered. Cleared on exiting the app.
     * @param context
     * @param password
     */
    public static void savePassword(Context context, String password){
        store(context, KEY_PASSWORD, password);
    }

    /**
     * Get the password the user has entered. Cleared on exiting the app.
     * @param context
     * @return
     */
    public static String getPassword(Context context){
        return get(context, KEY_PASSWORD);
    }

    /**
     * Stores a value in the shared preferences.
     * @param context
     * @param key
     * @param value
     */
    protected static void store(Context context, String key, String value){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        boolean saved = editor.commit();
        Log.d("SharedPrefs", "Saved "+key+": "+saved);
    }

    /**
     * Returns a stored value based on key.
     * @param context
     * @param key
     * @return
     */
    protected static String get(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        String restoredText = prefs.getString(key, null);

        return restoredText;
    }

    /**
     * Removes a specified key from the storage.
     * @param context
     * @param key one of the KEY_* constants of this class.
     * @return whether the removal was successful
     */
    public static boolean remove(Context context, String key){
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        return editor.commit();
    }
}
