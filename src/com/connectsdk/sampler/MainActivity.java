//
//  Connect SDK Sample App by LG Electronics
//
//  To the extent possible under law, the person who associated CC0 with
//  this sample app has waived all copyright and related or neighboring rights
//  to the sample app.
//
//  You should have received a copy of the CC0 legalcode along with this
//  work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
//

package com.connectsdk.sampler;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.connectsdk.core.AppInfo;
import com.connectsdk.device.ConnectableDevice;
import com.connectsdk.device.ConnectableDeviceListener;
import com.connectsdk.device.DevicePicker;
import com.connectsdk.discovery.DiscoveryManager;
import com.connectsdk.discovery.DiscoveryManager.PairingLevel;
import com.connectsdk.sampler.communicator.ServerCommunication;
import com.connectsdk.sampler.device.RegisteredDevice;
import com.connectsdk.sampler.epg.Channel;
import com.connectsdk.sampler.epg.ChannelListAdapter;
import com.connectsdk.sampler.epg.CustomTextView;
import com.connectsdk.sampler.epg.EPGData;
import com.connectsdk.sampler.epg.ProgramData;
import com.connectsdk.sampler.models.ServiceProvider;
import com.connectsdk.sampler.tasks.GetProviderDataTask;
import com.connectsdk.sampler.util.CallBack;
import com.connectsdk.sampler.util.CustomImageView;
import com.connectsdk.sampler.util.HttpUtil;
import com.connectsdk.sampler.util.ImageDownloader;
import com.connectsdk.sampler.util.ImageLoader;
import com.connectsdk.sampler.util.SharedPreferencesStorage;
import com.connectsdk.sampler.util.TestResponseObject;
import com.connectsdk.sampler.widget.AppAdapter;
import com.connectsdk.sampler.widget.EPGListAdapter;
import com.connectsdk.sampler.widget.SettingsListAdapter;
import com.connectsdk.service.DeviceService;
import com.connectsdk.service.DeviceService.PairingType;
import com.connectsdk.service.capability.KeyControl;
import com.connectsdk.service.capability.Launcher;
import com.connectsdk.service.command.ServiceCommandError;
import com.connectsdk.service.sessions.LaunchSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.util.InetAddressUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This MainActivity Class is MainScreen for StreamVision. Here shows List of Roku Devices, List of Installed Roku Device, List of EPG Channels and EPG Data
 */
public class MainActivity extends Activity {
    // key for logging
    private static final String LOG_TAG = "MainActivity";

//    public static final String CHANNEL_URL = "http://sv01.streamvisiontv.com/api/epg/channels";
//    public static final String EPG_URL = "http://sv01.streamvisiontv.com/api/epg";
//    public static final String CUSTOMER_DEV_URL = "http://sv01.streamvisiontv.com/api/customer/";

    // these urls are without the domain/IP part, that part is taken from the central CMS
    public static final String CHANNEL_URL = "api/epg/channels";
    public static final String EPG_URL = "api/epg";
    public static final String CUSTOMER_DEV_URL = "api/customer/";

    private static final long TIMER_INTERVAL = 10000;
    private static final long DAY_IN_MS = 24 * 60 * 60 * 1000;
    private static final int NUM_OF_DAYS = 3;
    private static final int MAX_ROW_WIDTH_IN_MINS = 24 * 60;
    private static boolean destroyed = false;
    private boolean disconnectStatusShown = false;

    /**
     * The currently selected service provider.
     */
    private ServiceProvider selectedProvider;

    static final long[] DATE_RANGE = new long[NUM_OF_DAYS];

    static {
        final long now = System.currentTimeMillis();
        for (int i = 0; i < NUM_OF_DAYS; i++) {
            DATE_RANGE[i] = now + (i * DAY_IN_MS);
        }
    }

    private IntentFilter s_intentFilter;
    private DateChangeReceiver dateChangeReceiver;

    private static final String TODAY = "Today";
    private static final String TOMORROW = "Tomorrow";


    private final Handler mHandler = new Handler();
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d("TEST_ROKU_2", "UPDATING INDICATOR" + mIndicator.getVisibility());
            if (mIndicator.getVisibility() == View.VISIBLE) {
                updateNowIndicatorPosition();
            }
        }
    };

    private Calendar mCalendar;
    private boolean dialogDismissed = false;

    private ProgressBar mProgressBar;
    private View mIndicator;
    private ListView mChannelsList;
    private HorizontalScrollView mTimeScrollWrapper;
    private HorizontalScrollView mProgramsScrollWrapper;
    private LinearLayout mProgramsWrapper;
    private LinearLayout mTimeWrapper;
    private LinearLayout mCachedTimeBar;
    private static int selectedDeviceIndex = -1;
    private static String selectedDeviceId = null;

    private int mDefaultMarginSize;

    private View mSelectedDate;
    private int mSelectedDateIndex;
    private boolean mIsFetchingContent;
    private ArrayList<EPGData> epgList = new ArrayList<EPGData>();
    private ArrayList<RegisteredDevice> serverDevicesList = new ArrayList<RegisteredDevice>();

    private Context mContext;
    private ArrayList<Channel> mChannels = new ArrayList<Channel>();

    private Button overTheAirButton;
    private Button closeEpgButton;
    //--Vivek
    private static ConnectableDevice mTV;
    AlertDialog dialog;
    AlertDialog pairingAlertDialog;
    AlertDialog pairingCodeDialog;
    ArrayList<Channel> channel = new ArrayList<Channel>();
    DevicePicker dp;

    TextView connectButton;
    SectionsPagerAdapter mSectionsPagerAdapter;

    private LaunchSession runningAppSession;
    private boolean streamVisionAppRunning = false;

    private static AppInfo appInfo;
    private String timeString;

    public static AppInfo getAppInfo() {
        return appInfo;
    }

    public static void setAppInfo(AppInfo appInfo) {
        MainActivity.appInfo = appInfo;
    }

    ViewPager mViewPager;
    ActionBar actionBar;

    private ConnectableDeviceListener deviceListener = new ConnectableDeviceListener() {

        @Override
        public void onPairingRequired(ConnectableDevice device, DeviceService service, PairingType pairingType) {
            Log.d("TEST_ROKU_6", "Connected to " + mTV.getIpAddress());

            switch (pairingType) {
                case FIRST_SCREEN:
                    Log.d("2ndScreenAPP", "First Screen");
                    pairingAlertDialog.show();
                    break;

                case PIN_CODE:
                    Log.d("2ndScreenAPP", "Pin Code");
                    pairingCodeDialog.show();
                    break;

                case NONE:
                default:
                    break;
            }
        }

        //Device Connection failed
        @Override
        public void onConnectionFailed(ConnectableDevice device, ServiceCommandError error) {
            Log.d("TEST_ROKU_6", "onConnectFailed");
            connectFailed(mTV);
            new Toast(getApplicationContext()).makeText(getApplicationContext(), "Device Connection Failed. Please select another Device.", Toast.LENGTH_LONG).show();
            Log.d("TEST_ROKU_6", "SHOW DEVICE DIALOG 2");
            showDeviceListDialog();
        }

        // Device Paired or Connected and ready to access the data
        @Override
        public void onDeviceReady(ConnectableDevice device) {
            Log.d("TEST_ROKU_6", "onPairingSuccess");
            if (pairingAlertDialog.isShowing()) {
                Log.d("TEST_ROKU_6", "onPairingSuccess");
                pairingAlertDialog.dismiss();
            }
            ArrayList<String> urls = new ArrayList<String>();
            urls.add(mTV.getIpAddress());
            new HttpUtil(urls, onValidatedConnection).execute();


        }


        /**
         * This function will call when the device disconnected
         * @param device
         */
        @Override
        public void onDeviceDisconnected(ConnectableDevice device) {
            Log.d("TEST_ROKU_6", "Device Disconnected");
            // connectEnded(mTV);
            Log.d("TEST_ROKU_6", " DISCONNECTED DEVICE");
            destroyed = true;
            if (System.currentTimeMillis() - lastConnected < 5000) {
                mHandler.removeCallbacks(deviceConnector);
                mHandler.postDelayed(deviceConnector, 2000);
            }
            connectButton.setText("Connect");
            selectedAppImage.setImageDrawable(null);

//           BaseFragment frag = mSectionsPagerAdapter.getFragment(mViewPager.getCurrentItem());
//            if (frag != null) {
//                Toast.makeText(getApplicationContext(), "Device Disconnected", Toast.LENGTH_SHORT).show();
//                frag.disableButtons();
//            }
            if(!disconnectStatusShown) {
                showErrorDialog("Please check your network or if the device is on.", false);
                disconnectStatusShown = true;
            }
        }

        @Override
        public void onCapabilityUpdated(ConnectableDevice device, List<String> added, List<String> removed) {

        }
    };
    private TestResponseObject testResponse;
    private Button rightButton;
    private Button okButton;
    private Button upButton;
    private Button leftButton;
    private Button downButton;
    private Button homeButton;
    private Button backButton;

    private Button playpauseButton;
    private Button rewindButton;
    private Button forwardButton;

    private Button refreshButton;
    private Button starButton;

    private ImageView streamVisionIcon;


    private GridView installedAppList;

    private ListView epgListview;

    private AppAdapter adapter;

    private EPGListAdapter epgAdapter;
    private Dialog deviceListDialog;

    private Button settingsButton;
    private LinearLayout mainLayout;
    Thread serverThread = null;
    public static final int SERVERPORT = 10000;

    private LinearLayout outterlayout;

    private ImageView selectedAppImage;
    private ImageLoader imageLoader;
    private ImageView share_btn;
    private ImageView reminder_btn;

    private boolean touchActivated = false;

    private boolean pauseMode = true;
    private boolean channelLoaded = false;

    private ProgramData currentlySelectedProgram = null;

    int height;
    int width;

    private static boolean[] epgLoadedAlready = new boolean[]{false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Register a date change broadcast receiver
        s_intentFilter = new IntentFilter();
        s_intentFilter.addAction(Intent.ACTION_TIME_TICK);
        s_intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        s_intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        dateChangeReceiver = new DateChangeReceiver();
        registerReceiver(dateChangeReceiver, s_intentFilter);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        testResponse = new TestResponseObject();
        setContentView(R.layout.home);
        imageLoader = new ImageLoader(this);
        //epgLoadedAlready = false;

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;

        mainLayout = (LinearLayout) findViewById(R.id.mainlayout);

        mainLayout.setVisibility(View.GONE);
        touchActivated = false;
        settingsButton = (Button) findViewById(R.id.settingsButton);
        connectButton = (TextView) findViewById(R.id.connect_button);

        streamVisionIcon = (ImageView) findViewById(R.id.streamvisionButton);

        playpauseButton = (Button) findViewById(R.id.playButton);
        playpauseButton.setOnClickListener(onPlayListener);

        rewindButton = (Button) findViewById(R.id.rewindButton);
        rewindButton.setOnClickListener(onRewindListener);

        forwardButton = (Button) findViewById(R.id.forwardButton);
        forwardButton.setOnClickListener(onForwardListener);

        refreshButton = (Button) findViewById(R.id.refreshbutton);
        forwardButton.setOnClickListener(onForwardListener);

        starButton = (Button) findViewById(R.id.starbutton);
        starButton.setOnClickListener(infoListener);
        overTheAirButton = (Button) findViewById(R.id.over_the_air_button);
        overTheAirButton.setOnClickListener(onOverTheAirClicked);

        outterlayout = (LinearLayout) findViewById(R.id.outterlayout);
        selectedAppImage = (ImageView) findViewById(R.id.selectedAppImage);
        selectedAppImage.setVisibility(View.GONE);
        connectButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("TEST_ROKU_6", "SHOW DEVICE DIALOG 3");
                if (isInternetAvailable()) {
                    showDeviceListDialog();
                } else {

                    showErrorDialog("Connection Error. Please check the device network!", false);

                }

//                    if (mTV != null && mTV.isConnected()){
//                        mTV.disconnect();
//                    }else{
//                        hConnectToggle();
//                    }

            }
        });

        pauseMode = true;

        installedAppList = (GridView) findViewById(R.id.installedapplist);

        epgListview = (ListView) findViewById(R.id.Epglist);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            epgListview.setFriction(ViewConfiguration.getScrollFriction() * 5);
        }


        rightButton = (Button) findViewById(R.id.rightbutton);
        rightButton.setOnTouchListener(onRightListener);
        upButton = (Button) findViewById(R.id.upbutton);
        upButton.setOnTouchListener(onUpListener);

        leftButton = (Button) findViewById(R.id.leftbutton);
        leftButton.setOnTouchListener(onLeftListener);

        okButton = (Button) findViewById(R.id.okbutton);
        okButton.setOnClickListener(onOkListener);
//        okButton.setOnTouchListener(onOkTouchListener);

        downButton = (Button) findViewById(R.id.downbutton);
        downButton.setOnTouchListener(onDownListener);

        homeButton = (Button) findViewById(R.id.homebutton);
        homeButton.setOnClickListener(onHomeListener);

        backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(onBackListener);

        closeEpgButton = (Button) findViewById(R.id.close_epg);
        closeEpgButton.setOnClickListener(onCloseEPGClicked);
        closeEpgButton.setVisibility(View.GONE);

        share_btn = (ImageView) findViewById(R.id.share_btn);
        reminder_btn = (ImageView) findViewById(R.id.reminder_btn);
        reminder_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentlySelectedProgram == null)
                    return;
                try {
                    setReminder();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        share_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentlySelectedProgram == null)
                    return;
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, currentlySelectedProgram.Title);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Liked this!");

                startActivity(Intent.createChooser(shareIntent, "StreamVision Android App."));

            }
        });

        streamVisionIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInternetAvailable()) {

                    final AppInfo appInfo = getAppInfo();

                    if (appInfo != null) {
                        if (mTV == null) {

                        }
                        mTV.getLauncher().launchAppWithInfo(appInfo, null, new Launcher.AppLaunchListener() {

                            @Override
                            public void onSuccess(LaunchSession session) {
                                if (selectedAppImage.getVisibility() == View.GONE) {
                                    selectedAppImage.setVisibility(View.VISIBLE);
                                }

                                String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/" + appInfo.getId();


                                //imageLoader.DisplayImage(imageUrl,selectedAppImage);
                                new ImageDownloader(selectedAppImage).execute(imageUrl);
                                setRunningAppInfo(session);
                                streamVisionAppRunning = true;
                            }

                            @Override
                            public void onError(ServiceCommandError error) {
                                if (selectedAppImage.getVisibility() == View.GONE) {
                                    selectedAppImage.setVisibility(View.VISIBLE);
                                }

                                String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/" + appInfo.getId();


                                //imageLoader.DisplayImage(imageUrl,selectedAppImage);
                                new ImageDownloader(selectedAppImage).execute(imageUrl);

                                streamVisionAppRunning = true;
                            }
                        });
                    }
                } else {

                    showErrorDialog("Connection Error. Please check the device network!", false);
                }
            }
        });

        // This will trigger when the user click roku apps from the streamvision android app.
        installedAppList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if (isInternetAvailable()) {
                    final AppInfo appInfo = (AppInfo) arg0.getItemAtPosition(arg2);
                    if (mTV == null && selectedDeviceIndex != -1) {
                        setSelectedDevice();
                        if (selectedDeviceId == null) {
                            Toast.makeText(getApplicationContext(), "Connected Roku Device went offline!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        mTV = list.get(selectedDeviceIndex);
                        mTV.addListener(deviceListener);
                        mTV.connect();
                        return;
                    } else if (mTV == null) {
                        Toast.makeText(getApplicationContext(), "Connected Roku Device went offline!", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (mTV.isConnected() == false) {
                        Toast.makeText(getApplicationContext(), "Unable to access device!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(mTV.getLauncher() == null)
                        return;

                    mTV.getLauncher().launchAppWithInfo(appInfo, null, new Launcher.AppLaunchListener() {

                        @Override
                        public void onSuccess(LaunchSession session) {
                            if (selectedAppImage.getVisibility() == View.GONE) {
                                selectedAppImage.setVisibility(View.VISIBLE);
                            }

                            String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/" + appInfo.getId();
                            new ImageDownloader(selectedAppImage).execute(imageUrl);
                            //imageLoader.DisplayImage(imageUrl,selectedAppImage);
                            setRunningAppInfo(session);
                            streamVisionAppRunning = false;
                        }

                        @Override
                        public void onError(ServiceCommandError error) {
                        }
                    });
                } else {

                    showErrorDialog("Connection Error. Please check the device network!", false);
                }
            }
        });

        epgListview.setFocusable(true);

        // This will trigger when the user click channels from the streamvision android app.
        epgListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int _position = position;
                if (isInternetAvailable()) {
                    if (streamVisionAppRunning) {
                        view.setSelected(true);
                        String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" + channel.get(position).getSvChannelId() + "_" + getLocalIpAddress();

                        ((AudioManager) getSystemService(getApplicationContext().AUDIO_SERVICE)).playSoundEffect(SoundEffectConstants.CLICK);
                        if (selectedAppImage.getVisibility() == View.GONE) {
                            selectedAppImage.setVisibility(View.VISIBLE);
                        }

                        new ImageDownloader(selectedAppImage).execute(channel.get(position).getChannelLogo());
                        //imageLoader.DisplayImage(imageUrl,selectedAppImage);
                        Log.d("URL ", url);

                        new DownloadWebPageTask(url).execute();
                    } else {
                        ((AudioManager) getSystemService(getApplicationContext().AUDIO_SERVICE)).playSoundEffect(SoundEffectConstants.CLICK);
                        final AppInfo appInfo = getAppInfo();

                        if (appInfo != null) {
                            if (mTV.getLauncher() == null)
                                return;

                            mTV.getLauncher().launchAppWithInfo(appInfo, null, new Launcher.AppLaunchListener() {

                                @Override
                                public void onSuccess(LaunchSession session) {
                                    if (selectedAppImage.getVisibility() == View.GONE) {
                                        selectedAppImage.setVisibility(View.VISIBLE);
                                    }

                                    String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/" + appInfo.getId();


                                    //imageLoader.DisplayImage(imageUrl,selectedAppImage);
                                    new ImageDownloader(selectedAppImage).execute(imageUrl);
                                    setRunningAppInfo(session);
                                    streamVisionAppRunning = true;
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" + channel.get(_position).getSvChannelId() + "_" + getLocalIpAddress();

                                            if (selectedAppImage.getVisibility() == View.GONE) {
                                                selectedAppImage.setVisibility(View.VISIBLE);
                                            }

                                            new ImageDownloader(selectedAppImage).execute(channel.get(_position).getChannelLogo());

                                            new DownloadWebPageTask(url).execute();

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(ServiceCommandError error) {
                                    Log.d("TEST_ROKU_9", error.getLocalizedMessage());
                                    String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" + channel.get(_position).getSvChannelId() + "_" + getLocalIpAddress();

                                    if (selectedAppImage.getVisibility() == View.GONE) {
                                        selectedAppImage.setVisibility(View.VISIBLE);
                                    }

                                    new ImageDownloader(selectedAppImage).execute(channel.get(_position).getChannelLogo());

                                    new DownloadWebPageTask(url).execute();
                                }
                            });
                        }
                    }
                } else {

                    showErrorDialog("Connection Error. Please check the device network!", false);
                }
            }
        });

        // This will trigger when the user click settings button from the streamvision android app.
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDeviceSettingsDialog();
            }
        });


        setupPicker();

        DiscoveryManager.getInstance().registerDefaultDeviceTypes();
        DiscoveryManager.getInstance().setPairingLevel(PairingLevel.ON);
        DiscoveryManager.getInstance().start();

        // check if we have a provider data and display it
        if(getSelectedProvider() != null){
            setProviderInfo(getSelectedProvider());
        }

        //   Toast.makeText(MainActivity.this,storedCustomerId,Toast.LENGTH_LONG).show();


        if (isInternetAvailable()) {
            password = SharedPreferencesStorage.getPassword(MainActivity.this);
            if (getSelectedProvider() != null && password != null) {
                customerId = SharedPreferencesStorage.getCustomerId(MainActivity.this);
                providerId = SharedPreferencesStorage.getProviderId(MainActivity.this);

                dialog.dismiss();
                if (mTV != null && mTV.isConnected()) {
                    mTV.disconnect();
                } else {
//                        hConnectToggle();
//                        getImageDevices();
                    Log.d(LOG_TAG, "IN HERE! "+password);
                    getListOfDevicesFromServer();

                    mainLayout.setVisibility(View.VISIBLE);
                    touchActivated = true;
                }
            } else {
                showCustomerLoginDialog();
            }
        } else {

            showErrorDialog("Connection Error. Please check the device network!", true);
        }
        new Handler().post(timeUpdater);
    }

    /**
     * Sets the provider logo to the appropriate image views.
     * @param image
     */
    private void setProviderLogo(Bitmap image) {
        ImageView providerLogoView = (ImageView) findViewById(R.id.providerLogo);
        providerLogoView.setImageBitmap(image);
    }

    /**
     * Sets the provider name to display next to the version number.
     * @param provider
     */
    private void setProviderInfo(ServiceProvider provider){
        TextView providerName = (TextView) findViewById(R.id.providerName);
        providerName.setText(provider.getName());
    }

    /**
     * This function is for set Remainder in calender for channels
     * @throws ParseException
     */
    public void setReminder() throws ParseException {
        Uri eventsUri, remainderUri;
        Cursor cursor;
        if (Build.VERSION.SDK_INT <= 7) {
            eventsUri = Uri.parse("content://calendar/events");
            remainderUri = Uri.parse("content://calendar/reminders");
            cursor = MainActivity.this.getContentResolver().query(
                    Uri.parse("content://calendar/calendars"),
                    new String[]{"_id", "displayName"}, null, null, null);

        } else if (Build.VERSION.SDK_INT <= 14) {
            eventsUri = Uri.parse("content://com.android.calendar/events");
            remainderUri = Uri
                    .parse("content://com.android.calendar/reminders");
            cursor = MainActivity.this.getContentResolver().query(
                    Uri.parse("content://com.android.calendar/calendars"),
                    new String[]{"_id", "displayName"}, null, null, null);

        } else {
            eventsUri = Uri.parse("content://com.android.calendar/events");
            remainderUri = Uri
                    .parse("content://com.android.calendar/reminders");
            cursor = MainActivity.this.getContentResolver().query(
                    Uri.parse("content://com.android.calendar/calendars"),
                    new String[]{"_id", "calendar_displayName"}, null, null,
                    null);

        }

        Log.e("count", "" + cursor.getColumnName(0));
        String[] calendarNames = new String[cursor.getCount()];
        String[] calendarId = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i = 0; i < calendarNames.length; i++) {
            calendarId[i] = cursor.getString(0);
            calendarNames[i] = cursor.getString(1);
            cursor.moveToNext();

        }

        long startCalTime;
        long endCalTime;
        Date eventDate = null;

        TimeZone timeZone = TimeZone.getDefault();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formattedDate = new SimpleDateFormat("MM/dd/yyyy");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, mSelectedDateIndex);  // number of days to add
        String strDate = (String) (formattedDate.format(c.getTime()));

        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
        String getO = sdfDate.format(currentlySelectedProgram.startTime);
        eventDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(strDate + " " + getO);
        cal.setTime(eventDate);

        startCalTime = cal.getTimeInMillis();

        SimpleDateFormat sdfDate2 = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
        String get1 = sdfDate.format(currentlySelectedProgram.endTime);
        eventDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(strDate + " " + get1);
        cal.setTime(eventDate);
        endCalTime = cal.getTimeInMillis();

        Log.d("TEST_ROKU_5", " Date is " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(eventDate));
        ContentValues event = new ContentValues();
        event.put(CalendarContract.Events.CALENDAR_ID, calendarId[0]);
        event.put(CalendarContract.Events.TITLE, currentlySelectedProgram.Title);
        event.put(CalendarContract.Events.DESCRIPTION, currentlySelectedProgram.Description);
        event.put(CalendarContract.Events.EVENT_LOCATION, "Eevnt Location");
        event.put(CalendarContract.Events.DTSTART, startCalTime);
        event.put(CalendarContract.Events.DTEND, endCalTime);
        event.put(CalendarContract.Events.STATUS, 1);
        event.put(CalendarContract.Events.HAS_ALARM, 1);
        event.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());

        Uri insertEventUri = MainActivity.this.getContentResolver().insert(
                eventsUri, event);

        ContentValues reminders = new ContentValues();
        reminders.put(CalendarContract.Reminders.EVENT_ID,
                Long.parseLong(insertEventUri.getLastPathSegment()));
        reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        reminders.put(CalendarContract.Reminders.MINUTES, 10);

        MainActivity.this.getContentResolver().insert(remainderUri, reminders);
        new Toast(getApplicationContext()).makeText(getApplicationContext(), " Reminder Set Successfully", Toast.LENGTH_LONG).show();

    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    System.out.println("ip1--:" + inetAddress);
                    System.out.println("ip2--:" + inetAddress.getHostAddress());
                    String ipv4;
                    // for getting IPV4 format
                    if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(ipv4 = inetAddress.getHostAddress())) {

                        String ip = inetAddress.getHostAddress().toString();
                        System.out.println("ip---::" + ip);

                        return ipv4;
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    private CallBack<ArrayList<Boolean>> onValidatedConnection = new CallBack<ArrayList<Boolean>>() {
        @Override
        public void execute(ArrayList<Boolean> response) {
            if (!response.get(0).booleanValue()) {
                new Toast(getApplicationContext()).makeText(getApplicationContext(), "Device you are trying to connect went offline! Please select again.", Toast.LENGTH_LONG).show();
                showDeviceListDialog();
            }
        }
    };
    private CallBack<ArrayList<Boolean>> onValidatedConnectionAndFetch = new CallBack<ArrayList<Boolean>>() {
        @Override
        public void execute(ArrayList<Boolean> response) {
            if (!response.get(0).booleanValue()) {
                new Toast(getApplicationContext()).makeText(getApplicationContext(), "Device you are trying to connect went offline! Please select again.", Toast.LENGTH_LONG).show();
                showDeviceListDialog();
            } else new RetrieveFeedTask().execute(getSelectedProvider().getCmsUrl()+CHANNEL_URL);
        }
    };


//    ViewTreeObserver.OnGlobalLayoutListener onDatel = new ViewTreeObserver.``OnGlobalLayoutListener() {
//        @Override
//        public void onGlobalLayout() {
//            Date present = new Date();
//            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
//            String getO = sdfDate.format(present);
//            ((HorizontalScrollView)findViewById(R.customerId.tv_guide_prog_scrollview)).smoothScrollTo( (int)( ( (stringToDate(strDate).getTime()-stringToDate("00:00:00").getTime())/ 60000) * 6)  , 0);
//           // mProgramsWrapper.getViewTreeObserver().removeOnGlobalLayoutListener(firstScroll);
//
//        }
//    };


    ViewTreeObserver.OnGlobalLayoutListener firstScroll = new ViewTreeObserver.OnGlobalLayoutListener() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onGlobalLayout() {
            Date present = new Date();
            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
            String strDate = sdfDate.format(present);
            if (((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).getScrollX() < 50)
                if (height <= 720) {
                    ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 4.5), 0);
                } else {
                    ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 6), 0);
                }

            else
                mProgramsWrapper.getViewTreeObserver().removeOnGlobalLayoutListener(firstScroll);
            // mProgramsWrapper.getViewTreeObserver().removeOnGlobalLayoutListener(firstScroll);

        }
    };
    ViewTreeObserver.OnGlobalLayoutListener secondScroll = new ViewTreeObserver.OnGlobalLayoutListener() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onGlobalLayout() {
            Date present = new Date();
            SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
            String strDate = sdfDate.format(present);
            if (height <= 720) {
                ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 4.5), 0);
            } else {
                ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 6), 0);
            }

            mProgramsWrapper.getViewTreeObserver().removeOnGlobalLayoutListener(secondScroll);
            // mProgramsWrapper.getViewTreeObserver().removeOnGlobalLayoutListener(firstScroll);

        }
    };

    /**
     *
     * @return the list devices which are in online currently
     */
    public List<ConnectableDevice> getImageDevices() {
        List<ConnectableDevice> imageDevices = new ArrayList<ConnectableDevice>();

        for (ConnectableDevice device : DiscoveryManager.getInstance().getCompatibleDevices().values()) {
            if (device.getModelName() != null && device.getModelName().toLowerCase().indexOf("roku") > -1)
                imageDevices.add(device);
            //Log.d("DeviceList ", imageDevices.size() + "" + device.getModelName());
        }

        return imageDevices;
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        if (dialog != null) {
            dialog.dismiss();
        }
        if (progressDlg != null) {
            progressDlg.dismiss();
        }

        if (mTV != null) {
            ///  Log.d("TEST_ROKU_6", "Calling Disconnect");
            destroyed = true;
            mTV.disconnect();
        }
        registerReceiver(dateChangeReceiver, s_intentFilter);

    }

    public void hConnectToggle() {
        if (!this.isFinishing()) {
            if (mTV != null) {
                if (mTV.isConnected())
                    mTV.disconnect();

                connectButton.setText("Connect");
                selectedAppImage.setImageDrawable(null);
                mTV.removeListener(deviceListener);
                mTV = null;
                for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
                    if (mSectionsPagerAdapter.getFragment(i) != null) {
                        mSectionsPagerAdapter.getFragment(i).setTv(null);
                    }
                }
            } else {
                dialog.show();
            }
        }
    }

    private void setupPicker() {
        dp = new DevicePicker(this);

        dialog = dp.getPickerDialog("Device List", new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                mTV = (ConnectableDevice) arg0.getItemAtPosition(arg2);
                mTV.addListener(deviceListener);
                mTV.connect();
                connectButton.setText(mTV.getFriendlyName());
                selectedAppImage.setImageDrawable(null);
                dp.pickDevice(mTV);

                Log.d("Response : ", mTV.getIpAddress());

            }
        });

        pairingAlertDialog = new AlertDialog.Builder(this)
                .setTitle("Pairing with TV")
                .setMessage("Please confirm the connection on your TV")
                .setPositiveButton("Okay", null)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dp.cancelPicker();

                        hConnectToggle();
                    }
                })
                .create();

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        pairingCodeDialog = new AlertDialog.Builder(this)
                .setTitle("Enter Pairing Code on TV")
                .setView(input)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (mTV != null) {
                            String value = input.getText().toString().trim();
                            mTV.sendPairingKey(value);
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dp.cancelPicker();

                        hConnectToggle();
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    }
                })
                .create();
    }
    //Vivek
    /**
     * this will call when the close button clicked in the epgscreen
     */
    private View.OnClickListener onCloseEPGClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            findViewById(R.id.frame_epg).setVisibility(View.GONE);
            findViewById(R.id.mainlayout).setVisibility(View.VISIBLE);
            findViewById(R.id.close_epg).setVisibility(View.GONE);
            closeEpgButton.setVisibility(View.GONE);
            connectButton.setVisibility(View.VISIBLE);
            touchActivated = true;
        }
    };

    /**
     * this will trigger when the overtheair button clicked in the mainscreen. It loads all epg data
     */
    private View.OnClickListener onOverTheAirClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (isInternetAvailable()) {
                findViewById(R.id.frame_epg).setVisibility(View.VISIBLE);
                findViewById(R.id.mainlayout).setVisibility(View.GONE);
                findViewById(R.id.close_epg).setVisibility(View.VISIBLE);
                closeEpgButton.setVisibility(View.VISIBLE);
                connectButton.setVisibility(View.GONE);
                touchActivated = false;
                Log.d("TEST_ROKU_2", epgLoadedAlready + " epgLoadedAlready ");
                if (!epgLoadedAlready[0]) {
                    if (epgList.size() > 0) {
                        Date present = new Date();
                        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
                        String strDate = sdfDate.format(present);
                        if (height <= 720) {
                            ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 4.5), 0);
                        } else {
                            ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 6), 0);
                        }

//                        ((HorizontalScrollView) findViewById(R.customerId.tv_guide_prog_scrollview)).smoothScrollTo((int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 6), 0);
                        return;
                    }
                    findViewById(R.id.sidebar).setVisibility(View.INVISIBLE);
                    final View parent = findViewById(R.id.tvguide_mainlayout);
                    final Resources res = getResources();
                    // Attempt to update the background
                    mProgressBar = (ProgressBar) parent.findViewById(R.id.progress_indicator);
                    mChannelsList = (ListView) parent.findViewById(R.id.tv_guide_listview);
                    mProgramsScrollWrapper = (HorizontalScrollView) parent.findViewById(R.id.tv_guide_prog_scrollview);
                    mTimeScrollWrapper = (HorizontalScrollView) parent.findViewById(R.id.tv_guide_time_scrollview);
                    mProgramsWrapper = (LinearLayout) parent.findViewById(R.id.tv_guide_program_data);
                    mTimeWrapper = (LinearLayout) findViewById(R.id.tv_guide_time_data);
                    ViewTreeObserver vto = mProgramsWrapper.getViewTreeObserver();
                    vto.addOnGlobalLayoutListener(firstScroll);
                    mIndicator = parent.findViewById(R.id.indicator);
                    // Update the channel list
                    mChannelsList.setBackgroundColor(Color.TRANSPARENT);
                    mProgramsScrollWrapper.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

                        @Override
                        public void onScrollChanged() {

                            int scrollX = mProgramsScrollWrapper.getScrollX(); //for horizontalScrollView
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                                mTimeScrollWrapper.setScrollX(scrollX);
                            }
                            //int scrollY = rootScrollView.getScrollY(); //for verticalScrollView
                            //DO SOMETHING WITH THE SCROLL COORDINATES

                        }
                    });
                    // Update the indicator
                    final TextView marker = (TextView) mIndicator.findViewById(R.id.marker);
                    marker.setText("Now");
                    marker.setBackgroundColor(Color.RED);
                    marker.setTextColor(Color.WHITE);
                    mIndicator.findViewById(R.id.line).setBackgroundColor(Color.RED);
                    // Reset preference value
                    initView();
                    epgLoadedAlready[0] = true;
                } else {
                    if (mSelectedDate == DateView[0]) {
                        Log.d("TEST_ROKU_5", " INSIDE SCROLL");
                        ViewTreeObserver vto = mProgramsWrapper.getViewTreeObserver();
                        vto.addOnGlobalLayoutListener(secondScroll);
                    } else
                        DateView[0].performClick();
                }
            }else{
                showErrorDialog("Connection Error. Please check the device network!",false);
            }
        }

    };

    //Vivek

    private void initView() {
//        mChannelsList.setDividerHeight(mDefaultMarginSize);
        mCalendar = Calendar.getInstance();
        mCalendar.setTime(new Date());

        if (true) {
            final long now = System.currentTimeMillis();
            for (int i = 0; i < NUM_OF_DAYS; i++) {
                DATE_RANGE[i] = now + (i * DAY_IN_MS);
            }

            populateDateBar((LinearLayout) findViewById(R.id.date_bar));

            // Fetch the data after everything has been initialized
            mProgressBar.setVisibility(View.VISIBLE);
//            fetchEPGData();
            fetchProgramData(0);
        }
        mHandler.postDelayed(mRunnable, TIMER_INTERVAL);
    }

    /**
     * This function update current time indicator UI
     */
    private void updateNowIndicatorPosition() {
        if (mSelectedDateIndex == 0) {
            final RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mIndicator.getLayoutParams();
            lp.leftMargin = getOriginX();
            mIndicator.setVisibility(View.VISIBLE);
            mIndicator.requestLayout();
            mHandler.postDelayed(mRunnable, TIMER_INTERVAL);
        }
    }

    /**
     * FetchEPGData from the API
     */
    private void fetchEPGData() {
        mIsFetchingContent = true;
        new RetrieveFeedTask().execute(getSelectedProvider().getCmsUrl()+CHANNEL_URL);

    }

    ProgressDialog progressDlg = null;

    /**
     * Set the service provider the user has selected (gotten from central API based on entered data).
     * @return
     */
    public ServiceProvider getSelectedProvider() {
        if(selectedProvider == null){
            selectedProvider = SharedPreferencesStorage.getServiceProvider(MainActivity.this);
        }
        return selectedProvider;
    }

    /**
     * Get the service provider the user has selected (gotten from central API based on entered data).
     * @param selectedProvider
     */
    public void setSelectedProvider(ServiceProvider selectedProvider) {
        SharedPreferencesStorage.saveServiceProvider(MainActivity.this, selectedProvider);
        this.selectedProvider = selectedProvider;
    }

    /**
     * This class download EPGData from the API in Background AsyncTask.
     */
    class RetrieveFeedTask extends AsyncTask<String, Void, String> {

        private String result;
        private String url;

        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.main_progress_indicator).setVisibility(View.VISIBLE);
//            progressDlg = new ProgressDialog(MainActivity.this);
//            progressDlg.setMessage("Loading ...");
//            progressDlg.setCancelable(false);
//            progressDlg.show();
        }

        protected String doInBackground(String... urls) {
            try {
                url = urls[0];
                result = getJSON(urls[0]);
            } catch (Exception e) {
                Log.d("TEST_ROKU", "ERROR FETCH");
                return null;
            }
            return result;
        }

        protected void onPostExecute(String feed) {

            if (isInternetAvailable()) {

                if (feed != null && !feed.equals("Error")) {
                    Log.d("Feed", feed);

                    if (url.equals(getSelectedProvider().getCmsUrl()+CHANNEL_URL)) {

                        registerSuccess(parseChannelData(feed));
                        channelLoaded = true;
                    } else {
                        parseEPGResponse(feed);

                    }

                } else {

                    showErrorDialog("Server Error. Please try again later.", false);

                }
            } else {

                showErrorDialog("Connection Error. Please check the device network!", true);
            }
            if (MainActivity.this.isFinishing() || Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && MainActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                return;
            }
            findViewById(R.id.main_progress_indicator).setVisibility(View.GONE);
//            if(progressDlg.isShowing())
//            progressDlg.dismiss();
        }
    }

    /**
     * This function will parse json EPG Response
     * @param JSONResponse
     */
    private void parseEPGResponse(String JSONResponse) {
        Log.d("TEST_ROKU_7", "PARSING EPG RESPONSE");
        mChannelsList.setAdapter(new ChannelListAdapter(getApplicationContext(), channel));
        mChannelsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {

            }
        });
        epgList = new ArrayList<EPGData>();
        mChannelsList.setVisibility(View.VISIBLE);
        try {
            JSONObject mainResponse = new JSONObject(JSONResponse);

            JSONArray epgArray = mainResponse.getJSONArray("data");
            JSONObject epgTemp;
            for (int i = 0; i < epgArray.length(); i++) {
                epgTemp = epgArray.getJSONObject(i);
                JSONArray showsArray;
                if (epgTemp.has("shows"))
                    showsArray = epgTemp.getJSONArray("shows");
                else
                    showsArray = new JSONArray();
                EPGData ed = new EPGData();
                ed.channelId = epgTemp.getString("channelId");
                ed.channelNumber = epgTemp.getString("channelNumber");
                ed.svChannelId = epgTemp.getString("svChannelId");

                for (int j = 0; j < showsArray.length(); j++) {
                    ProgramData pd = new ProgramData();
                    pd.Title = showsArray.getJSONObject(j).getString("Title");
                    pd.Description = showsArray.getJSONObject(j).getString("Description");
                    String startDate = utcToDate(showsArray.getJSONObject(j).getString("startTimeUtc"));
                    String endDate = utcToDate(showsArray.getJSONObject(j).getString("endTimeUtc"));

                    Log.d("Formated Date ", startDate.substring(11, 19));


                    pd.startTime = stringToDate(startDate.substring(11, 19));
                    pd.endTime = stringToDate(endDate.substring(11, 19));
                    ed.programList.add(pd);

                }
                epgList.add(ed);
            }
            Log.d("TEST_ROKU_7", "TOTAL EPG LIST ADDED" + epgList.size());
        } catch (JSONException e) {
            Log.d("TEST_ROKU_7", "ERROR EPG ADDITION" + e.getMessage());
            e.printStackTrace();
        }
        findViewById(R.id.sidebar).setVisibility(View.VISIBLE);
        if (mSelectedDateIndex == 0) updateNowIndicatorPosition();
        //mChannels = channels;


        mIsFetchingContent = false;
        populateEpgGrid();

    }

    /**
     * Convert the String to Date
     * @param timeStr
     * @return Date
     */
    private Date stringToDate(String timeStr) {
        Calendar now = Calendar.getInstance();
        TimeZone tz = now.getTimeZone();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        //Log.d("TEST_ROKU", "INCOMING DATE " + timeStr );
        Date date = null;
        try {
            sdf.setTimeZone(tz);

            date = sdf.parse(timeStr);
            Log.d("Formated Date ", date + " Displayed date");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Convert UTC time to Local Time
     * @param timeStr
     * @return local time as String
     */
    private String utcToDate(String timeStr) {

        Calendar cal = Calendar.getInstance();
        TimeZone t = cal.getTimeZone();
        Log.d("Formated Date", " = " + t.getDisplayName());


        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar now = Calendar.getInstance();
        TimeZone tz = now.getTimeZone();
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);
        String result = destFormat.format(parsed);
        Log.d("Formated Date ", timeStr + "   Before");
        Log.d("Formated Date ", result);
        return result;
    }

    /**
     * convert date to time for the given Date
     * @param dNow
     * @return time as String
     */
    private String getTime(Date dNow) {
        String time;
        SimpleDateFormat ft =
                new SimpleDateFormat("HH:mm a");

        return time = ft.format(dNow);
    }

    /**
     * Function Draws Cell for the EPG Data
     * @param view
     * @param cellDuration
     */
    private void processCellDimensions(final View view, final int cellDuration) {
        int width = 0;
        if (height <= 720) {
            width = 300 * cellDuration / 60;
        } else {

            width = 400 * cellDuration / 60;
        }

        final LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
        lp.width = width - mDefaultMarginSize;
        lp.rightMargin = mDefaultMarginSize;
    }

    /**
     * This Draws new Row for every Channels
     * @return
     */
    private LinearLayout addNewRow() {
        final LinearLayout row = new LinearLayout(getApplicationContext());
        row.setOrientation(LinearLayout.HORIZONTAL);
        mProgramsWrapper.addView(row, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return row;
    }

    /**
     * This function draws time row for epg data
     * @return
     */
    private LinearLayout addTimeRow() {
        final LinearLayout row = new LinearLayout(getApplicationContext());
        row.setOrientation(LinearLayout.HORIZONTAL);
        mTimeWrapper.addView(row, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return row;
    }

    private void generateAndAddTimeBar() {
        if (mCachedTimeBar == null) {
            final LinearLayout row = addTimeRow();
            //         row.setBackgroundColor(manager.getColor(ColorKey.GUIDE_BACKGROUND_TIMEBAR));
            final int height = 70;
            final int textColor = Color.WHITE;
            final String format = "%02d:%02d";
            for (int i = 0; i < (24 * 2); i++) {
                String meridian_AM = " AM";
                String meridian_PM = " PM";

                final TextView time = new CustomTextView(getApplicationContext());
                if (height <= 720) {
                    time.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                } else {
                    time.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                }

                time.setGravity(Gravity.CENTER_VERTICAL);
                time.setLayoutParams(new LinearLayout.LayoutParams(0, height));
                time.setPadding(2, 0, 0, 0);
                if (i >= 24) {
                    if (((i - 24) / 2) == 0) {

                        time.setText(String.format(Locale.getDefault(), format, 12, ((i - 24) % 2) * 30) + meridian_PM);
                        Log.d("TEST_ROKU_5", String.format(Locale.getDefault(), format, 12, ((i - 12) % 2) * 30) + meridian_PM);

                    } else {

                        time.setText(String.format(Locale.getDefault(), format, (i - 24) / 2, ((i - 24) % 2) * 30) + meridian_PM);
                        Log.d("TEST_ROKU_5", String.format(Locale.getDefault(), format, (i - 24) / 2, ((i - 12) % 2) * 30) + meridian_PM);
                    }


                } else {

                    if ((i / 2 == 0)) {

                        time.setText(String.format(Locale.getDefault(), format, 12, (i % 2) * 30) + meridian_AM);

                    } else {
                        time.setText(String.format(Locale.getDefault(), format, i / 2, (i % 2) * 30) + meridian_AM);
                    }

                    Log.d("TEST_ROKU_5", String.format(Locale.getDefault(), format, i / 2, (i % 2) * 30) + meridian_AM);
                }

                time.setTextColor(textColor);
                processCellDimensions(time, 30);
                row.addView(time);
            }
            mCachedTimeBar = row;
        } else {
            //mProgramsWrapper.addView(mCachedTimeBar);
        }
    }

    /**
     * This function will populate EPG Grid for each channel
     */
    private void populateEpgGrid() {
        mChannels = channel;
        mChannelsList.setVisibility(View.VISIBLE);

        Log.d("WidhtHeight", " height " + height + "-width---->  " + width);
        int rowHeight = 0;
        if (height <= 720) {
            rowHeight = 66;
        } else {

            rowHeight = 126;
        }

        mIsFetchingContent = false;
        generateAndAddTimeBar();
        Channel currentChannel = null;
        for (int i = 0; i < mChannels.size(); i++) {
            int j = i;
            currentChannel = mChannels.get(i);
            Log.d("TEST_ROKU_5", " FROM EPG GRID " + i + "  " + currentChannel.getChannelLogo());
            for (j = 0; j < epgList.size(); j++) {
                if (mChannels.get(i).getSvChannelId().equals(epgList.get(j).svChannelId)) {

                    break;
                }

            }
            if (j == epgList.size() || epgList.get(j).programList.size() == 0) {
                LinearLayout row = addNewRow();
                ProgramData pd = new ProgramData();
                pd.imageURL = currentChannel.getChannelLogo();
                pd.startTime = stringToDate("00:00:00");
                pd.endTime = stringToDate("23:59:00");
                pd.Title = " ";
                pd.svChannelId = currentChannel.getSvChannelId();

                generateAndAddCell(pd, rowHeight, (int) ((pd.endTime.getTime() - pd.startTime.getTime()) / 60000), row, 0);
                continue;
            }
            final List<ProgramData> programDataList = epgList.get(j).programList;
            //final List<Integer> durationsList = DateManager.getProgramsInterval(index, epgDataMap);

            LinearLayout row = addNewRow();
            int durationListIndex = 0;
            int rowWidth = 0;
            if (programDataList.size() > 0 && !programDataList.get(0).startTime.equals(stringToDate("00:00:00"))) {
                //rowWidth=(int)(programDataList.get(0).startTime.getTime() - stringToDate("00:00:00").getTime())/60000;
                ProgramData pd = new ProgramData();
                pd.imageURL = currentChannel.getChannelLogo();
                pd.startTime = stringToDate("00:00:00");
                pd.endTime = programDataList.get(0).startTime;
                pd.Title = " ";
                pd.svChannelId = currentChannel.getSvChannelId();

                generateAndAddCell(pd, rowHeight, (int) ((pd.endTime.getTime() - pd.startTime.getTime()) / 60000), row, rowWidth);
            }

            for (int k = 0; k < programDataList.size(); k++) {
                int duration;
                ProgramData programData = programDataList.get(k);
                programDataList.get(k).imageURL = currentChannel.getChannelLogo();
                programDataList.get(k).svChannelId = currentChannel.getSvChannelId();
                Log.d("TEST_ROKU_4", " programData.imageURL " + programData.imageURL);

                Log.d("StartTime ", ((programData.endTime.getTime() - programData.startTime.getTime()) / 60000) + "");
                if (programData.endTime.before(programData.startTime))
                    duration = (int) ((stringToDate("24:00:00").getTime() - programData.startTime.getTime()) / 60000);
                else
                    duration = (int) ((programData.endTime.getTime() - programData.startTime.getTime()) / 60000);

                if (rowWidth >= MAX_ROW_WIDTH_IN_MINS) {
                    // Start a new row
                    row = addNewRow();
                    rowWidth = 0;
                }

                rowWidth += duration;
                if (programData == programDataList.get(0))
                    generateAndAddCell(programData, rowHeight, duration, row, 0);
                else
                    generateAndAddCell(programData, rowHeight, duration, row, 0);
                durationListIndex++;
            }
        }
        Date present = new Date();
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
        String strDate = sdfDate.format(present);

        if (height < 720) {

            ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).scrollTo((int) (300 * ((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 3600000)) - 300, 0);

        } else {

            ((HorizontalScrollView) findViewById(R.id.tv_guide_prog_scrollview)).scrollTo((int) (400 * ((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 3600000)) - 400, 0);
        }

        mProgressBar.setVisibility(View.INVISIBLE);
    }

    /**
     * This function Draw All Detail about the channel shows in UI
     * @param programData
     * @param rowHeight
     * @param duration
     * @param row
     * @param isFirst
     */
    private void generateAndAddCell(final ProgramData programData, final int rowHeight, final int duration, final LinearLayout row, final int isFirst) {
        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, rowHeight);
        lp.bottomMargin = mDefaultMarginSize;
        //lp.leftMargin = 400 * isFirst / 60;
        final int paddingTopBottom = 10;
        final int paddingLeftRight = 10;
        final TextView cell = new CustomTextView(getApplicationContext());
        cell.setLayoutParams(lp);
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
        Date present = new Date();
        String strDate = sdfDate.format(present);
        if (programData.Title.equals(" "))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                cell.setBackground(getResources().getDrawable(R.drawable.border_inactive));
            }
        else if (programData.startTime.before(stringToDate(strDate)) && programData.endTime.after(stringToDate(strDate))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                cell.setBackground(getResources().getDrawable(R.drawable.border_inactive));
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                cell.setBackground(getResources().getDrawable(R.drawable.border_inactive));
            }
        }
        cell.setGravity(Gravity.CENTER_VERTICAL);
        cell.setEllipsize(TextUtils.TruncateAt.END);
        cell.setSingleLine(true);
        cell.setPadding(paddingLeftRight, paddingTopBottom, paddingLeftRight, paddingTopBottom);
        cell.setText(programData.Title);
        processCellDimensions(cell, duration);
        Log.d("Test_ROKU_5", "Setting " + programData.imageURL + " for " + programData.Title + " on Channel Id " + programData.channelId);
        // Attempt to set the background color to the appropriate value, based on time of the program
        try {
            final long start = programData.startTime.getTime();
            final long end = start + duration * 60 * 1000;
            final long now = System.currentTimeMillis();
            /*if (start <= now && now <= end) {
                cell.setBackgroundColor(configs().getColor(ColorKey.GUIDE_BACKGROUND_CURRENT));
            } else {
                cell.setBackgroundColor(defaultColor);
            }*/
        } catch (NumberFormatException e) {
            //cell.setBackgroundColor(defaultColor);
        }
        ((CustomTextView) cell).setGestureListener(new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDown(MotionEvent e) {
                currentlySelectedProgram = programData;
                TextView title = (TextView) findViewById(R.id.sidebar_title);
                TextView sidebar_desc = (TextView) findViewById(R.id.sidebar_desc);
                TextView sidebar_DESC = (TextView) findViewById(R.id.sidebar_DESC);
                TextView sidebar_time = (TextView) findViewById(R.id.sidebar_time);
                title.setText(programData.Title);
                String desc = programData.Description;

                sidebar_desc.setText(desc);

//                if (!desc.equals("")) {
//                    sidebar_desc.setVisibility(View.VISIBLE);
//                    sidebar_DESC.setVisibility(View.VISIBLE);
//                    sidebar_desc.setText(desc);
//                } else {
//                    sidebar_desc.setVisibility(View.GONE);
//                    sidebar_DESC.setVisibility(View.GONE);
//                }

                sidebar_desc.setMovementMethod(new ScrollingMovementMethod());
                sidebar_time.setText(getTime(programData.startTime) + " - " + getTime(programData.endTime));
                new ImageDownloader((ImageView) findViewById(R.id.sidebar_image)).execute(programData.imageURL);
                ((CustomImageView) findViewById(R.id.sidebar_image)).setGestureListener(new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }

                    // event when double tap occurs
                    @Override
                    public boolean onDoubleTap(MotionEvent e) {
                        float x = e.getX();
                        float y = e.getY();

                        String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" + programData.svChannelId + "_" + getLocalIpAddress();
                        Log.d("TEST_ROKU_5", " URL CALLED " + url);

                        new DownloadWebPageTask(url).execute();
                        return true;
                    }
                });

                return true;
            }


            // event when double tap occurs
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                float x = e.getX();
                float y = e.getY();
                currentlySelectedProgram = programData;

                Log.d("Double Tap", "Tapped at: (" + x + "," + y + ")");
                Date present = new Date();
                SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
                String strDate = sdfDate.format(present);
                if (programData.startTime.before(stringToDate(strDate)) && programData.endTime.after(stringToDate(strDate))) {
                    String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" + programData.svChannelId + "_" + getLocalIpAddress();

                    Log.d("TEST_ROKU_5", " URL CALLED " + url);

                    new DownloadWebPageTask(url).execute();
                }
                ((TextView) findViewById(R.id.sidebar_title)).setText(programData.Title);
                ((TextView) findViewById(R.id.sidebar_desc)).setText(programData.Description);
                new ImageDownloader((ImageView) findViewById(R.id.sidebar_image)).execute(programData.imageURL);
                ((CustomImageView) findViewById(R.id.sidebar_image)).setGestureListener(new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }

                    // event when double tap occurs
                    @Override
                    public boolean onDoubleTap(MotionEvent e) {
                        float x = e.getX();
                        float y = e.getY();

                        String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" + programData.svChannelId + "_" + getLocalIpAddress();
                        Log.d("TEST_ROKU_5", " URL CALLED " + url);

                        new DownloadWebPageTask(url).execute();
                        return true;
                    }
                });

                return true;
            }
        });
        /*cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Date present = new Date();
                SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
                String strDate = sdfDate.format(present);
                if (programData.startTime.before(stringToDate(strDate)) && programData.endTime.after(stringToDate(strDate)))
                {
                    String url = "http://" + mTV.getIpAddress() + ":8060/input?op=" +programData.svChannelId + "_" + getLocalIpAddress();

                    Log.d("TEST_ROKU_5", " URL CALLED " + url);

                    new DownloadWebPageTask(url).execute();
                }
                ((TextView) findViewById(R.customerId.sidebar_title)).setText(programData.Title);
                ((TextView) findViewById(R.customerId.sidebar_desc)).setText(programData.Description);
                new ImageDownloader((ImageView)findViewById(R.customerId.sidebar_image)).execute(programData.imageURL);
            }
        });*/
        row.addView(cell);
    }


    public String getJSON(String address) {
        StringBuilder builder = new StringBuilder();
        HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = 30000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 30000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        HttpClient client = new DefaultHttpClient(httpParameters);
        HttpGet httpGet = new HttpGet(address);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                builder.append("Error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            if(mIndicator.getVisibility() == View.VISIBLE)
                mIndicator.setVisibility(View.GONE);
            if(mProgressBar.getVisibility() == View.VISIBLE)
                mProgressBar.setVisibility(View.VISIBLE);
            showErrorDialog("Server Error. Please try again later.", false);
        }
        //Log.d("TEST_ROKU", builder.toString());
        return builder.toString();
    }
/*
    private void parseChannelResponse(String channels) {
        try {
            JSONArray channelsArray = new JSONArray(channels);
            JSONObject channelTemp;
            for (int i=0 ; i<channelsArray.length();i++) {
                channelTemp = channelsArray.getJSONObject(i);
                Chan cd = new ChannelData();
                cd.name = channelTemp.getString("channelId");
                cd.url= channelTemp.getString("channelLogo");
                mChannels.add(cd);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //mChannels = channels;
        mChannelsList.setAdapter(new ChannelListAdapter(getApplicationContext(), mChannels));
        mChannelsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long customerId) {

            }
        });
        mChannelsList.setVisibility(View.VISIBLE);
        mIsFetchingContent = false;

    }*/

    private int getOriginX() {
        Date present = new Date();
        SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
        String strDate = sdfDate.format(present);
        if (height <= 720) {
            return (int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 4.98);
        } else {
            return (int) (((stringToDate(strDate).getTime() - stringToDate("00:00:00").getTime()) / 60000) * 6.61);
        }

    }

    private String oldCustomerId = "", oldProviderId = "";
    private View[] DateView = new View[3];

    /**
     * Populate Date bar in the UI for Today Tomorow as button
     * @param container
     */
    private void populateDateBar(final LinearLayout container) {
        final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final Locale locale = Locale.getDefault();
        container.removeAllViews();
        mSelectedDate = null;
        for (int i = 0; i < NUM_OF_DAYS; i++) {
            mCalendar.setTimeInMillis(DATE_RANGE[i]);
            final StringBuilder builder = new StringBuilder();
            if (i == 0) {
                builder.append("Today").append(" ( ");
            } else if (i == 1) {
                builder.append("Tomorrow").append(" ( ");
            } else if (i == 2) {
                builder.append(mCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale)).append(" ( ");
            }
            builder.append(mCalendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, locale)).append(" ").append(mCalendar.get(Calendar.DATE));
            builder.append(" )");
            final View view = inflater.inflate(R.layout.tv_guide_date, container, false);
            DateView[i] = view;
            if (view instanceof TextView) {
                view.setBackgroundColor(Color.BLACK);
                ((TextView) view).setText(builder.toString());
                // Set the first view as the selected view by default
                if (mSelectedDate == null) {
                    view.setSelected(true);
                    view.setBackgroundColor(Color.GRAY);
                    mSelectedDate = view;
                }
                // Set the click listener
                final int index = i;
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        onDateClicked(index, v);
                    }
                });
                container.addView(view);
            } else {
                Log.e("TVGuide", "Inflated view isn't of the right type: " + view.getClass().getName());
            }
        }
        mSelectedDateIndex = 0;
    }

    private void onDateClicked(final int index, final View view) {
        // Only trigger this if the provided index is not the same as the selected index,
        // and if the app is not fetching content
        Log.d("TEST_ROKU_2", "mselected Index" + mSelectedDateIndex + " Index " + index);
        if (index != mSelectedDateIndex && !mIsFetchingContent) {
            // Update the views
            mIndicator.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mChannelsList.setVisibility(View.INVISIBLE);
            if (mSelectedDate != null) {
                mSelectedDate.setSelected(false);
                mSelectedDate.setBackgroundColor(Color.BLACK);
            }
            if (view != null) {
                view.setSelected(true);
                mSelectedDate = view;
                mSelectedDate.setBackgroundColor(Color.GRAY);
            }
            mSelectedDateIndex = index;
            // Clear the contents
            clearGuideContents();
            // Fetch the content
            if (index == 0) {
                ViewTreeObserver vto = mProgramsWrapper.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(firstScroll);
            }
            fetchProgramData(index);
        }
    }

    private void fetchProgramData(final int index) {
        mIsFetchingContent = true;
        String s = getURLParams(index);

        Log.d("FEED ", s);
        new RetrieveFeedTask().execute(getSelectedProvider().getCmsUrl()+EPG_URL + s);

//        new RetrieveFeedTask().execute(EPG_URL + "?startTimeUTC=" + getUTCDate(strDate) + "T"+getUTCTime(strDate)+"&endTimeUTC=" + getUTCDate(strDate) + "T24:00:00");

    }

    /**
     * This function will provide starttime and endtime for getting epg data in UTC format from the local timezone
     * @param index
     * @return
     */
    private String getURLParams(int index) {
        String result = "";
        TimeZone tz = TimeZone.getDefault();
        int offset = tz.getRawOffset();
        Log.d("FEED ", offset + "offset");
        String timeZone = String.format("%s%02d%02d", offset >= 0 ? "+" : "-", offset / 3600000, (offset / 60000) % 60);

        if (index == 0) {
            if (offset > 0) {
                int hours = offset / 3600000;
                int minutes = (offset / 60000) % 60;
                if (minutes > 0)
                    hours++;
                String dateString = decrementDay();
//            String timeString = (24-hours) + ":" +(60-minutes)+":00";
                String timeString;
                if (minutes > 0)
                    timeString = (24 - hours) + ":" + (60 - minutes) + ":00";
                else timeString = (24 - hours) + ":" + "00:00";

                //new RetrieveFeedTask().execute(EPG_URL + "?startTimeUTC=" + dateString + "T"++"&endTimeUTC=" + endDate + "T"+startTime);
                result = "?startTimeUTC=" + dateString + "T" + timeString + "&endTimeUTC=" + currentDay(0) + "T" + timeString;
                Log.d("FEED ", result + "result1");
            } else if (offset <= 0) {
                offset = 0 - (offset);
                int hours = offset / 3600000;
                int minutes = (offset / 60000) % 60;
                String dateString = incrementDay(1);
                String timeString;
                if (minutes < 10)
                    timeString = "0" + (minutes) + ":00";
                else
                    timeString = minutes + ":00";
                if (hours < 10)
                    timeString = "0" + hours + ":" + timeString;
                else
                    timeString = hours + ":" + timeString;
                result = "?startTimeUTC=" + currentDay(0) + "T" + timeString + "&endTimeUTC=" + dateString + "T" + timeString;
                Log.d("FEED ", result + "result2");
            }
        } else if (index == 1) {

            if (offset > 0) {
                int hours = offset / 3600000;
                int minutes = (offset / 60000) % 60;
                if (minutes > 0)
                    hours++;
                String dateString = decrementTwoDay();
//            String timeString = (24-hours) + ":" +(60-minutes)+":00";
                String timeString;
                if (minutes > 0)
                    timeString = (24 - hours) + ":" + (60 - minutes) + ":00";
                else timeString = (24 - hours) + ":" + "00:00";

                //new RetrieveFeedTask().execute(EPG_URL + "?startTimeUTC=" + dateString + "T"++"&endTimeUTC=" + endDate + "T"+startTime);
                result = "?startTimeUTC=" + dateString + "T" + timeString + "&endTimeUTC=" + currentDay(1) + "T" + timeString;
                Log.d("FEED ", result + "result1");
            } else if (offset <= 0) {
                offset = 0 - (offset);
                int hours = offset / 3600000;
                int minutes = (offset / 60000) % 60;
                String dateString = incrementDay(2);
                String timeString;
                if (minutes < 10)
                    timeString = "0" + (minutes) + ":00";
                else
                    timeString = minutes + ":00";
                if (hours < 10)
                    timeString = "0" + hours + ":" + timeString;
                else
                    timeString = hours + ":" + timeString;
                result = "?startTimeUTC=" + currentDay(1) + "T" + timeString + "&endTimeUTC=" + dateString + "T" + timeString;
                Log.d("FEED ", result + "result2");
            }

        } else if (index == 2) {

            if (offset > 0) {
                int hours = offset / 3600000;
                int minutes = (offset / 60000) % 60;
                if (minutes > 0)
                    hours++;
                String dateString = decrementThreeDay();
//            String timeString = (24-hours) + ":" +(60-minutes)+":00";
                String timeString;
                if (minutes > 0)
                    timeString = (24 - hours) + ":" + (60 - minutes) + ":00";
                else timeString = (24 - hours) + ":" + "00:00";

                //new RetrieveFeedTask().execute(EPG_URL + "?startTimeUTC=" + dateString + "T"++"&endTimeUTC=" + endDate + "T"+startTime);
                result = "?startTimeUTC=" + dateString + "T" + timeString + "&endTimeUTC=" + currentDay(2) + "T" + timeString;
                Log.d("FEED ", result + "result1");
            } else if (offset <= 0) {
                offset = 0 - (offset);
                int hours = offset / 3600000;
                int minutes = (offset / 60000) % 60;
                String dateString = incrementDay(3);
                String timeString;
                if (minutes < 10)
                    timeString = "0" + (minutes) + ":00";
                else
                    timeString = minutes + ":00";
                if (hours < 10)
                    timeString = "0" + hours + ":" + timeString;
                else
                    timeString = hours + ":" + timeString;
                result = "?startTimeUTC=" + currentDay(2) + "T" + timeString + "&endTimeUTC=" + dateString + "T" + timeString;
                Log.d("FEED ", result + "result2");
            }
        }
        return result;
    }

    /**
     * function provide current date from int
     * @param day
     * @return String current date
     */
    private String currentDay(int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        if (day != 0) {
            cal.add(Calendar.DATE, day);
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Log.d("TEST_TIME", dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }

    /**
     * This function increment one day for the given int value
     * @param day
     * @return incremented date as string
     */
    private String incrementDay(int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, day);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Log.d("TEST_TIME", dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }

    /**
     * This function decreament one days
     * @return
     */
    private String decrementDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Log.d("TEST_TIME", "PREVIOUS " + dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }

    /**
     * This function decreament two days
     * @return
     */
    private String decrementTwoDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        cal.setTime(cal.getTime());
        cal.add(Calendar.DATE, -1);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Log.d("TEST_TIME", "PREVIOUS " + dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }

    /**
     * This function decreament three days
     * @return
     */
    private String decrementThreeDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 2);
        cal.setTime(cal.getTime());
        cal.add(Calendar.DATE, -1);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Log.d("TEST_TIME", "PREVIOUS " + dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }


    private void clearGuideContents() {
        mProgramsWrapper.removeAllViewsInLayout();
    }


    private String getIncreamentedDate(String dte) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dte));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 1);  // number of days to add
        String dt = sdf.format(c.getTime());  // dt is now the new date

        return dt;
    }


    private String getUTCDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date value = null;
        try {
            value = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatter.setTimeZone(TimeZone.getDefault());
        String dt = dateFormatter.format(value);

        return dt;
    }


    static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date GetUTCdatetimeAsDate() {
        //note: doesn't check for null
        return StringDateToDate(GetUTCdatetimeAsString());
    }

    public static String GetUTCdatetimeAsString() {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    public static Date StringDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try {
            dateToReturn = (Date) dateFormat.parse(StrDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateToReturn;
    }


    //--Vivek

    /**
     * Right Button from the remote
     */
    private View.OnTouchListener onRightListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                if (mTV != null && mTV.hasCapability(KeyControl.Right)) {
                    if (mTV.getCapability(KeyControl.class) != null) {
                        mTV.getCapability(KeyControl.class).right(null);

                        outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
                        testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.RightClicked);

                    }
                } else {
                    //disableButton(rightButton);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        outterlayout.setBackgroundResource(R.drawable.pressed);
                    }
                }, 100);
            }
            return false;
        }
    };

    /**
     * UP Button from the remote
     */
    private View.OnTouchListener onUpListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                    mTV.getCapability(KeyControl.class).up(null);

                    outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
                    testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.UpClicked);


                } else {
                    //disableButton(upButton);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        outterlayout.setBackgroundResource(R.drawable.pressed);
                    }
                }, 100);
            }
            return false;
        }
    };

    /**
     * DOWN Button from the remote
     */
    private View.OnTouchListener onDownListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                    mTV.getCapability(KeyControl.class).down(null);

                    outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
                    testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.DownClicked);


                } else {
                    //disableButton(upButton);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        outterlayout.setBackgroundResource(R.drawable.pressed);
                    }
                }, 100);
            }
            return false;
        }
    };

    /**
     * HOME Button from the remote
     */
    private View.OnClickListener onHomeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectedAppImage.setImageDrawable(null);
            if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                mTV.getCapability(KeyControl.class).home(null);
                testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.HomeClicked);
            } else {
                //disableButton(upButton);
            }
        }
    };

    /**
     * BACK Button from the remote
     */
    private View.OnClickListener onBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                mTV.getCapability(KeyControl.class).back(null);
            } else {
                //disableButton(upButton);
            }
        }
    };


    /**
     * Info Button from the remote
     */
    private View.OnClickListener infoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mTV == null || mTV.getIpAddress() == null)
                return;

            String url = "http://" + mTV.getIpAddress() + ":8060//keypress/info";

            Log.d("URL ", url);

            new DownloadWebPageTask(url).execute();
        }
    };

    /**
     * LEFT Button from the remote
     */
    private View.OnTouchListener onLeftListener = new View.OnTouchListener() {


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                    mTV.getCapability(KeyControl.class).left(null);
                    outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
                    testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.LeftClicked);


//            } else {
                    // disableButton(leftButton);
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        outterlayout.setBackgroundResource(R.drawable.pressed);
                    }
                }, 100);
            }
            return false;
        }
    };

    /**
     * Rewind Button from the remote
     */
    private View.OnClickListener onRewindListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mTV == null || mTV.getIpAddress() == null)
                return;

            String url = "http://" + mTV.getIpAddress() + ":8060//keypress/rev";

            Log.d("URL ", url);

            new DownloadWebPageTask(url).execute();
        }
    };

    /**
     * Forward Button from the remote
     */
    private View.OnClickListener onForwardListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mTV == null || mTV.getIpAddress() == null)
                return;

            String url = "http://" + mTV.getIpAddress() + ":8060//keypress/fwd";

            Log.d("URL ", url);

            new DownloadWebPageTask(url).execute();
        }
    };

    /**
     * PlayPause Button from the remote
     */
    private View.OnClickListener onPlayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (pauseMode) {

                playpauseButton.setBackgroundResource(R.drawable.play);
                pauseMode = false;

            } else {
                pauseMode = true;
                playpauseButton.setBackgroundResource(R.drawable.pause);
            }

            if(mTV == null || mTV.getIpAddress() == null)
                return;

            String url = "http://" + mTV.getIpAddress() + ":8060//keypress/play";

            Log.d("URL ", url);

            new DownloadWebPageTask(url).execute();

        }
    };

    /**
     * OK Button from the remote
     */
    private View.OnClickListener onOkListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                mTV.getCapability(KeyControl.class).ok(null);
                testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.Clicked);
            } else {
                //disableButton(clickButton);
            }
        }
    };

    private View.OnTouchListener onOkTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (mTV != null && mTV.getCapability(KeyControl.class) != null) {
                mTV.getCapability(KeyControl.class).ok(null);
                testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.Clicked);
            } else {
                //disableButton(clickButton);
            }
            return false;
        }
    };


//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        int x = (int) event.getX();
//        int y = (int) event.getY();
//        if (touchActivated) {
//        switch (event.getAction()) {
//
//
//
//                case MotionEvent.ACTION_DOWN:
//                    Log.d("Touched", "ACTION_DOWN");
//                    if ((x > 943 && x < 1124) && (y > 598 && y < 619)) {
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
//                            }
//                        },1);
//                    } else if ((x > 1191 && x < 1219) && (y > 694 && y < 840)) {
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
//                            }
//                        }, 1);
//
//
//                    } else if ((x > 836 && x < 851) && (y > 699 && y < 866)) {
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
//                            }
//                        }, 1);
//
//
//                    } else if ((x > 932 && x < 1138) && (y > 945 && y < 970)) {
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                outterlayout.setBackgroundResource(R.drawable.arrowbutton_bg);
//                            }
//                        }, 1);
//
//
//
//                    }
//                case MotionEvent.ACTION_UP:
//
//                    Log.d("Touched", "ACTION_UP");
//                    if ((x > 943 && x < 1124) && (y > 598 && y < 619)) {
//
//                        Log.d("Touched", "Up Button");
//                        Log.d("Touched", "x : " + x + " y : " + y);
//
//                        if (mTV.getCapability(KeyControl.class) != null) {
//                            mTV.getCapability(KeyControl.class).up(null);
//                            testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.UpClicked);
//                            outterlayout.setBackgroundResource(R.drawable.pressed);
//                        } else {
//                            //disableButton(upButton);
//                        }
//
//                    } else if ((x > 1191 && x < 1219) && (y > 694 && y < 840)) {
//
//                        Log.d("Touched", "right Button");
//                        Log.d("Touched", "x : " + x + " y : " + y);
//
//                        if (mTV.hasCapability(KeyControl.Right)) {
//                            if (mTV.getCapability(KeyControl.class) != null) {
//                                mTV.getCapability(KeyControl.class).right(null);
//
//                                testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.RightClicked);
//                                outterlayout.setBackgroundResource(R.drawable.pressed);
//                            }
//                        } else {
//                            //disableButton(rightButton);
//                        }
//                    } else if ((x > 836 && x < 851) && (y > 699 && y < 866)) {
//
//                        Log.d("Touched", "Left Button");
//                        Log.d("Touched", "x : " + x + " y : " + y);
//
//                        if (mTV.getCapability(KeyControl.class) != null) {
//                            mTV.getCapability(KeyControl.class).left(null);
//                            testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.LeftClicked);
//
//                            outterlayout.setBackgroundResource(R.drawable.pressed);
//                        }
//                    } else if ((x > 932 && x < 1138) && (y > 945 && y < 970)) {
//
//                        Log.d("Touched", "Down Button");
//                        Log.d("Touched", "x : " + x + " y : " + y);
//                        if (mTV.getCapability(KeyControl.class) != null) {
//                            mTV.getCapability(KeyControl.class).down(null);
//
//                            testResponse = new TestResponseObject(true, TestResponseObject.SuccessCode, TestResponseObject.DownClicked);
//                            outterlayout.setBackgroundResource(R.drawable.pressed);
//                        } else {
//                            //disableButton(upButton);
//                        }
//
//                    }
//                }
//        }
//        return false;
//    }

    private long lastConnected;

    @Override
    protected void onResume() {

        super.onResume();
        Log.d("TEST_ROKU_6", "ON RESUME CALLED");
        if (selectedDeviceIndex != -1) {
            //  mTV.disconnect();
            Log.d("TEST_ROKU_6", "ON RESUME CONNECT AGAIN CALLED");
            list = getImageDevices();
            mTV = list.get(selectedDeviceIndex);
            mTV.addListener(deviceListener);
            mTV.connect();
            lastConnected = System.currentTimeMillis();

        }
        destroyed = false;
    }

    void registerSuccess(ArrayList<Channel> feedChannels) {
        Log.d("2ndScreenAPP", "successful register");
        Log.d("2ndScreenAPP", mTV.getConnectedServiceNames());
        String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/";
        Log.d("url ", imageUrl);
        adapter = new AppAdapter(MainActivity.this, R.layout.list_item, imageUrl);
        installedAppList.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        channel = feedChannels;

        epgAdapter = new EPGListAdapter(MainActivity.this, channel);
        epgListview.setAdapter(epgAdapter);

        if (mTV.hasCapability(Launcher.Application_List)) {
            mTV.getLauncher().getAppList(new Launcher.AppListListener() {

                @Override
                public void onSuccess(List<AppInfo> appList) {
                    adapter.clear();
                    for (int i = 0; i < appList.size(); i++) {
                        final AppInfo app = appList.get(i);

                        if (app.getName().equals("StreamVision")) {
                            //Log.d("AppName ", app.getName());
                            String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/";
                            setAppInfo(app);
                            //imageLoader.DisplayImage(imageUrl + app.getId(), streamVisionIcon);
                            new ImageDownloader(streamVisionIcon).execute(imageUrl + app.getId());
                            continue;

                        }
                        adapter.add(app);
                    }

                    adapter.sort();
                }

                @Override
                public void onError(ServiceCommandError error) {
                }
            });
        }
        // BaseFragment frag = mSectionsPagerAdapter.getFragment(mViewPager.getCurrentItem());
        //if (frag != null)
        //  frag.setTv(mTV);
    }


    void registerSuccess() {
        String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/";
        adapter = new AppAdapter(MainActivity.this, R.layout.list_item, imageUrl);
        installedAppList.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        epgAdapter = new EPGListAdapter(MainActivity.this, channel);
        epgListview.setAdapter(epgAdapter);

        if (mTV.hasCapability(Launcher.Application_List)) {
            mTV.getLauncher().getAppList(new Launcher.AppListListener() {

                @Override
                public void onSuccess(List<AppInfo> appList) {
                    adapter.clear();
                    for (int i = 0; i < appList.size(); i++) {
                        final AppInfo app = appList.get(i);

                        if (app.getName().equals("StreamVision")) {
                            //Log.d("AppName ", app.getName());
                            String imageUrl = "http://" + mTV.getIpAddress() + ":8060/query/icon/";
                            setAppInfo(app);
                            //imageLoader.DisplayImage(imageUrl + app.getId(), streamVisionIcon);
                            new ImageDownloader(streamVisionIcon).execute(imageUrl + app.getId());
                            continue;

                        }
                        adapter.add(app);
                    }

                    adapter.sort();
                }

                @Override
                public void onError(ServiceCommandError error) {
                }
            });
        }
        // BaseFragment frag = mSectionsPagerAdapter.getFragment(mViewPager.getCurrentItem());
        //if (frag != null)
        //  frag.setTv(mTV);
    }


    void connectFailed(ConnectableDevice device) {
        if (device != null)
            Log.d("2ndScreenAPP", "Failed to connect to " + device.getIpAddress());

        if (mTV != null) {
            mTV.removeListener(deviceListener);
            Log.d("TEST_ROKU_6", "Calling Disconnect from Connect Failed");
            mTV.disconnect();
            mTV = null;
        }
    }

    void connectEnded(ConnectableDevice device) {
        if (pairingAlertDialog.isShowing()) {
            pairingAlertDialog.dismiss();
        }
        mTV.removeListener(deviceListener);
        mTV = null;
    }


    public void setRunningAppInfo(LaunchSession session) {
        runningAppSession = session;
    }


    public ArrayList<Channel> parseChannelData(String json) {

        Log.d("JSON ", json);

        ArrayList<Channel> channelList = new ArrayList<Channel>();
        try {


            JSONObject obj = new JSONObject(json);

//            String success = obj.getString("success");
//            if(!obj.isNull("message")) {
//                String message = obj.getString("message");
//                Log.d("JSON ",message);
//
//            }
//            Log.d("JSON ",success);

            JSONArray m_jArry = obj.optJSONArray("data");

            Log.d("Details-->", m_jArry.length() + "");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString("channelId"));
                String channelId = jo_inside.getString("channelId");
                String channelNumber = jo_inside.getString("channelNumber");
                Log.d("Details-->", jo_inside.getString("channelNumber"));
                String description = jo_inside.getString("description");
                Log.d("Details-->", jo_inside.getString("description"));
                String svChannelId = jo_inside.getString("svChannelId");
                Log.d("Details-->", jo_inside.getString("svChannelId"));
                String channelLogo = jo_inside.getString("channelLogo");
                Log.d("Details-->", jo_inside.getString("channelLogo"));

                //Add your values in your `ArrayList` as below:


                channelList.add(new Channel(channelId, channelNumber, description, channelLogo, svChannelId));
                //Same way for other value...
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return channelList;

    }


    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {


        String url;

        DownloadWebPageTask(String url) {
            this.url = url;
        }

        @Override
        protected String doInBackground(String... urls) {

            try {


                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url);
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("", ""));
                    UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
                    post.setEntity(ent);
                    HttpResponse responsePOST = client.execute(post);
                    HttpEntity resEntity = responsePOST.getEntity();
                    if (resEntity != null) {
                        Log.d("RESPONSE", EntityUtils.toString(resEntity));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {


            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }
    }

    private ProgressDialog progressDialog;


    /**
     * Get All Registered Device from the API in background and listed in the UI
     */
    private class GetAllRegisterDevice extends AsyncTask<String, Void, List<RegisteredDevice>> {
        private boolean serverError = false;


        @Override
        protected void onPreExecute() {

            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Loading Registered Devices...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            super.onPreExecute();
        }

        String url;

//
//        GetAllRegisterDevice(String url) {
//            this.url = url;
//        }

        @Override
        protected List<RegisteredDevice> doInBackground(String... urls) {

            List<RegisteredDevice> deviceList = new ArrayList<RegisteredDevice>();

            try {
                try {
                    JSONArray arrayJson = ServerCommunication.getJSONfromURL(getSelectedProvider().getCmsUrl()+CUSTOMER_DEV_URL + customerId +"/"+password+"/devices");
                    if(arrayJson == null) {
                        serverError = true;
                        return deviceList;
                    }

                    Log.d("JSON Object ", arrayJson.toString());
                    for (int i = 0; i < arrayJson.length(); i++) {

                        JSONObject jsonobj = arrayJson.getJSONObject(i);
                        Log.d("JSON Object ", "DeviceID : " + i + " = " + jsonobj.getString("DeviceID"));
                        Log.d("JSON Object ", "AccountID : " + i + " = " + jsonobj.getString("AccountID"));
                        Log.d("JSON Object ", "Identifier : " + i + " = " + jsonobj.getString("Identifier"));
                        Log.d("JSON Object ", "ServiceType : " + i + " = " + jsonobj.getString("ServiceType"));

                        String deviceId = jsonobj.getString("DeviceID");
                        String accountId = jsonobj.getString("AccountID");
                        String identifier = jsonobj.getString("Identifier");
                        String serviceType = jsonobj.getString("ServiceType");

                        RegisteredDevice device = new RegisteredDevice(deviceId, accountId, identifier, serviceType);
                        deviceList.add(device);
                    }
                    // RegisteredDevice device = new RegisteredDevice("1GU44M002307", "2234", "MY HOME", "PLATINUM");
                    //deviceList.add(device);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {

            }
            return deviceList;
        }

        @Override
        protected void onPostExecute(List<RegisteredDevice> result) {

            progressDialog.dismiss();
            if(serverError == true) {
                new Handler().post(serverErrorRunnable);
                return;
            }

            if (result != null && result.size() > 0) {
//                if (mTV != null) {
//                    mTV.disconnect();
//                }
                serverDevicesList = new ArrayList<RegisteredDevice>();
                if (listview != null && listview.getAdapter() != null)
                    ((SettingsListAdapter) listview.getAdapter()).notifyDataSetChanged();
                Log.d("JSON Object ", "size : " + " = " + result.size());

                for (RegisteredDevice device : result) {

                    Log.d("JSON Object ", "DeviceID : " + " = " + device.getDeviceId());
                    Log.d("JSON Object ", "AccountID : " + " = " + device.getAccountId());
                    Log.d("JSON Object ", "Identifier : " + " = " + device.getIdentifier());
                    Log.d("JSON Object ", "ServiceType : " + " = " + device.getServiceType());
                    // serverDevicesList.add(device.getDeviceId());
                }
                serverDevicesList.addAll(result);
            } else {
                if (oldCustomerId != "") {
                    customerId = oldCustomerId;
                    if (oldProviderId != "") {
                        providerId = oldProviderId;
                    }
                    new Handler().post(alertRunnable2);
                }
            }
            if (serverDevicesList.isEmpty()) {

                // Toast.makeText(getApplicationContext(), " NO DEVICES FOUND!", Toast.LENGTH_LONG);
                new Handler().post(alertRunnable);
                mainLayout.setVisibility(View.VISIBLE);
                touchActivated = true;
                dialog.dismiss();
            } else {
                Log.d("TEST_ROKU_6", "SHOW DEVICE DIALOG 1.1");
                if (deviceListDialog == null || !deviceListDialog.isShowing())
                    showDeviceListDialog();
                else {
                    setSelectedDevice();
                }
            }

        }
    }

    /**
     * Fields for the login dialog.
     */
    private String customerId;
    private String password, providerId;
    private EditText customerIdField;
    private EditText passwordField;
    private EditText providerIdField;

    private void showCustomerLoginDialog() {

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customerlogin_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        customerIdField = (EditText) dialog.findViewById(R.id.customerId); // gets the last name
        passwordField = (EditText) dialog.findViewById(R.id.password);
        providerIdField = (EditText) dialog.findViewById(R.id.providerId);

        customerId = SharedPreferencesStorage.getCustomerId(MainActivity.this);
        providerId = SharedPreferencesStorage.getProviderId(MainActivity.this);
        customerIdField.setText(customerId);
        providerIdField.setText(providerId);

        Button dialogButton = (Button) dialog.findViewById(R.id.OK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerId = customerIdField.getText().toString();
                password = passwordField.getText().toString();
                providerId = providerIdField.getText().toString();

                Log.d("Button ", "pressed  " + customerId);

                if (customerId != null && password != null && providerId != null &&
                    !customerId.equals("") && !password.equals("") && !providerId.equals("")) {

                    dialog.dismiss();

                    // contact central API and get provider info
                    final GetProviderDataTask getProvTask = new GetProviderDataTask(MainActivity.this);
                    getProvTask.setTaskListener(new GetProviderDataTask.TaskListener(){
                        @Override
                        public void onFinished(ServiceProvider result) {
                            // if the request failed, display a message and input form again
                            if(result == null){
                                String message = "Incorrect data! Please check the entered data and try again.";
                                if(getProvTask.getLastError() != null){
                                    message =  getProvTask.getLastError();
                                }
                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                                showCustomerLoginDialog();
                            }else{
                                setSelectedProvider(result);
                                SharedPreferencesStorage.saveCustomerId(MainActivity.this, customerId);
                                SharedPreferencesStorage.saveProviderId(MainActivity.this, providerId);
                                SharedPreferencesStorage.savePassword(MainActivity.this, password);

                                setProviderInfo(result);

                                // continue as planned
                                if (mTV != null && mTV.isConnected()) {
                                    mTV.disconnect();
                                } else {
                                    getListOfDevicesFromServer();

                                    mainLayout.setVisibility(View.VISIBLE);
                                    touchActivated = true;
                                }
                            }
                        }
                    });
                    getProvTask.execute(providerId);
                }else{
                    Toast.makeText(getApplicationContext(), "You must fill in all the fields!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }

    /**
     * This Function Calls AsyncTask to get the registeredDevice
     */
    private void getListOfDevicesFromServer() {

        if(password == null || password.equals("")){
            showCustomerLoginDialog();
        }else {
            new GetAllRegisterDevice().execute();
        }
    }

    private ListView listview;
    List<ConnectableDevice> list;

    private void setSelectedDevice() {
        if (selectedDeviceId == null)
            return;
        for (int i = 0; i < list.size(); i++) {
            //for(int j=0; i<myList.size();j++) {
            //if (registeredList.get(i).
            //}
            list.get(i).getId();
            Iterator itr = list.get(i).getServices().iterator();
            while (itr.hasNext()) {
                DeviceService ds = (DeviceService) itr.next();

                Log.d("TEST_ROKU_3", "  " + list.get(i).getIpAddress());
                list.get(i).setModelNumber(
                        ds.getServiceConfig().getServiceUUID().substring(ds.getServiceConfig().getServiceUUID().indexOf("ecp:") + 4));
                //if(registeredList)
            }
            String stringToCompare = list.get(i).getModelNumber();
            if (stringToCompare.equals(selectedDeviceId)) {
                selectedDeviceIndex = i;
                break;
            }

        }
        if (selectedDeviceIndex == -1) {
            if (selectedDeviceId != null)
                Toast.makeText(getApplicationContext(), "Roku Device [customerId:" + selectedDeviceId + "] went offline!", Toast.LENGTH_SHORT).show();
            selectedDeviceId = null;
            selectedDeviceIndex = -1;
            if (mTV != null)
                mTV.disconnect();
            return;
        } else {
            for (int i = 0; i < serverDevicesList.size(); i++) {
                ((RegisteredDevice) (listview.getAdapter().getItem(i))).isSelected = ((RegisteredDevice) (listview.getAdapter().getItem(i))).getDeviceId().equals(selectedDeviceId);
                ((SettingsListAdapter) listview.getAdapter()).notifyDataSetChanged();
            }
        }
    }

    /**
     * This function shows List of devices in the Dialog
     */
    private void showDeviceListDialog() {

        Button refreshButoon;
        Button backButton;
        if (deviceListDialog != null && deviceListDialog.isShowing())
            return;


        final Dialog dialog = new Dialog(this);
        deviceListDialog = dialog;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_devicelist_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dp = new DevicePicker(this);

        refreshButoon = (Button) dialog.findViewById(R.id.refreshbutton_);
        backButton = (Button) dialog.findViewById(R.id.backButton_);
        listview = (ListView) dialog.findViewById(R.id.custom_deviceList);
//        SharedPreferencesStorage.clear(MainActivity.this);

        SharedPreferencesStorage.saveCustomerId(MainActivity.this, customerId);
        SharedPreferencesStorage.saveProviderId(MainActivity.this, providerId);
        SharedPreferencesStorage.savePassword(MainActivity.this, password);
//        SharedPreferencesStorage.saveServiceProvider(MainActivity.this, getSelectedProvider());


        backButton.setVisibility(View.VISIBLE);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        refreshButoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getListOfDevicesFromServer();

            }
        });

        list = new ArrayList<ConnectableDevice>();

        List<String> deviceName = new ArrayList<String>();

        for (ConnectableDevice device : list) {

            deviceName.add(device.getFriendlyName());
        }

        Log.d(LOG_TAG, "Creating Settings list adapter");
        SettingsListAdapter adapter = new SettingsListAdapter(this, list, serverDevicesList);
        listview.setAdapter(adapter);
        if (mTV != null) {
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        //finish();
                        dialog.dismiss();
                        dialogDismissed = true;
                    }
                    return false;
                }
            });
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RegisteredDevice s = (RegisteredDevice) listview.getAdapter().getItem(position);

                if (isInternetAvailable()) {

                    selectedDeviceIndex = -1;
                    selectedDeviceId = s.getDeviceId();
                    for (int i = 0; i < list.size(); i++) {
                        //for(int j=0; i<myList.size();j++) {
                        //if (registeredList.get(i).
                        //}
                        list.get(i).getId();
                        Iterator itr = list.get(i).getServices().iterator();
                        while (itr.hasNext()) {
                            DeviceService ds = (DeviceService) itr.next();

                            Log.d("TEST_ROKU_3", "  " + ds.getServiceConfig().getServiceUUID());
                            list.get(i).setModelNumber(
                                    ds.getServiceConfig().getServiceUUID().substring(ds.getServiceConfig().getServiceUUID().indexOf("ecp:") + 4));
                            //if(registeredList)
                        }
                        String stringToCompare = list.get(i).getModelNumber();
                        if (stringToCompare.equals(s.getDeviceId())) {
                            selectedDeviceIndex = i;
                            break;
                        }

                    }
                    if (selectedDeviceIndex == -1) {
                        Toast.makeText(getApplicationContext(), "Device is Offline.", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        for (int i = 0; i < serverDevicesList.size(); i++) {
                            ((RegisteredDevice) (listview.getAdapter().getItem(i))).isSelected = (i == position);
                            ((SettingsListAdapter) listview.getAdapter()).notifyDataSetChanged();
                        }
                    }

//                dp.pickDevice(mTV);
                    if (((RegisteredDevice) (listview.getAdapter().getItem(position))).getIdentifier() != null &&
                            ((RegisteredDevice) (listview.getAdapter().getItem(position))).getIdentifier().length() > 0) {
                        connectButton.setText(((RegisteredDevice) (listview.getAdapter().getItem(position))).getIdentifier());
                        selectedAppImage.setImageDrawable(null);
                    }
                    else {
                        connectButton.setText(((RegisteredDevice) (listview.getAdapter().getItem(position))).getDeviceId());
                        selectedAppImage.setImageDrawable(null);
                    }
                    mainLayout.setVisibility(View.VISIBLE);
                    touchActivated = true;
                    dialog.dismiss();
                    ConnectToDevice();
                }else{
                    showErrorDialog("Connection Error. Please check the device network!",false);
                }
            }

        });

        dialog.show();
        mHandler.postDelayed(deviceRefresher, 0);
    }

    private void ConnectToDevice() {

        mTV = list.get(selectedDeviceIndex);
        mTV.addListener(deviceListener);
        mTV.connect();
        ArrayList<String> urls = new ArrayList<String>();
        urls.add(mTV.getIpAddress());
        new HttpUtil(urls, onValidatedConnectionAndFetch).execute();

    }

    private ListView settingsListview;
    private Button settings_cancelButton;
    private Button settings_updateButton;
    private EditText settingsCustomerIdField, settingsProviderIdField, settingsPasswordField;

    private void showDeviceSettingsDialog() {

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.settings_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dp = new DevicePicker(this);

        settings_cancelButton = (Button) dialog.findViewById(R.id.settings_cancelButton);
        settings_updateButton = (Button) dialog.findViewById(R.id.settings_updateButton);
        settingsCustomerIdField = (EditText) dialog.findViewById(R.id.settings_customerIdEditText);
        settingsProviderIdField = (EditText) dialog.findViewById(R.id.settings_providerIdEditText);
        settingsPasswordField = (EditText) dialog.findViewById(R.id.settings_passwordEditText);

        settingsCustomerIdField.setText(customerId);
        settingsProviderIdField.setText(providerId);

        settings_cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        settings_updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldCustomerId = customerId;
                oldProviderId = providerId;
                String enteredCustomerId = settingsCustomerIdField.getText().toString();
                String enteredProviderId = settingsProviderIdField.getText().toString();
                String enteredPassword = settingsPasswordField.getText().toString();
                Log.d("Button ", "pressed  " + customerId);

                if (enteredCustomerId != null && enteredPassword != null && enteredProviderId != null &&
                        !enteredCustomerId.equals("") && !enteredPassword.equals("") && !enteredProviderId.equals("")) {
                    customerId = settingsCustomerIdField.getText().toString();
                    providerId = settingsProviderIdField.getText().toString();
                    password = settingsPasswordField.getText().toString();

                    dialog.dismiss();

                    // contact central API and get provider info
                    final GetProviderDataTask getProvTask = new GetProviderDataTask(MainActivity.this);
                    getProvTask.setTaskListener(new GetProviderDataTask.TaskListener(){
                        @Override
                        public void onFinished(ServiceProvider result) {
                            // if the request failed, display a message and input form again
                            if(result == null){
                                String message = "Incorrect data! Please check the entered data and try again.";
                                if(getProvTask.getLastError() != null){
                                    message =  getProvTask.getLastError();
                                }
                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                                showCustomerLoginDialog();
                            }else{
                                setSelectedProvider(result);
                                SharedPreferencesStorage.saveCustomerId(MainActivity.this, customerId);
                                SharedPreferencesStorage.saveProviderId(MainActivity.this, providerId);
                                SharedPreferencesStorage.savePassword(MainActivity.this, password);

                                // update branding logo
                                setProviderInfo(result);


                                // continue as planned
                                if (mTV != null && mTV.isConnected()) {
                                    getListOfDevicesFromServer();

                                    mainLayout.setVisibility(View.VISIBLE);
                                    touchActivated = true;
                                } else {
                                    getListOfDevicesFromServer();

                                    mainLayout.setVisibility(View.VISIBLE);
                                    touchActivated = true;
                                }
                            }
                        }
                    });
                    getProvTask.execute(providerId);
                }else{
                    Toast.makeText(getApplicationContext(), "You must fill in all the fields!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }


    /**
     * If some thing went wrong like network not connected server down means this dialog will shows.
     * @param message
     * @param finish
     */
    private void showErrorDialog(final String message, final boolean finish) {

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        dialog.setCanceledOnTouchOutside(false);

        Button okButton = (Button) dialog.findViewById(R.id.OK);

        TextView errormessage = (TextView) dialog.findViewById(R.id.errormessage);
        errormessage.setText(message);
        TextView errorHeader = (TextView) dialog.findViewById(R.id.header);

        if(message.matches("Please check your network or if the device is on.")){
            errorHeader.setText("Lost Connection To Device");
        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(message.matches("Please check your network or if the device is on."))
                    disconnectStatusShown = false;

                dialog.dismiss();
                if (finish) {

                    finish();
                    ;
                }
            }
        });
        dialog.show();
    }

    private ServerSocket serverSocket;
    String adMessageFromClient = "";
    Handler reconnectHandler = new Handler();

    class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;
            try {
                serverSocket = new ServerSocket(SERVERPORT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {

                    socket = serverSocket.accept();

                    CommunicationThread commThread = new CommunicationThread(socket);
                    new Thread(commThread).start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        public void run() {
            char[] read = new char[1024];

            try {

                input.read(read, 0, 1024);
                String incomingString = String.valueOf(read);
                Log.d("TEST_ROKU_3", incomingString);
                Log.d("TEST_ROKU_3", incomingString.substring(incomingString.indexOf("url=") + 4, incomingString.indexOf("HTTP") - 2));
                mHandler.postDelayed(mRunnable3, 0);
                mHandler.postDelayed(mRunnable2, 30000);
                input.close();

                //updateConversationHandler.post(new updateUIThread(read));

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    class updateUIThread implements Runnable {
        private String msg;

        public updateUIThread(String str) {
            this.msg = str;
        }

        @Override
        public void run() {
            adMessageFromClient += this.msg;
            Log.d("TEST_ROKU_2", adMessageFromClient);
        }
    }

    private final Runnable mRunnable2 = new Runnable() {
        @Override
        public void run() {
            findViewById(R.id.ad_window).setVisibility(View.GONE);
        }
    };

    private final Runnable alertRunnable2 = new Runnable() {
        @Override
        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("No Registered Devices Found! Retaining previous data.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            //  dismissDialog(customerId);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    };

    private final Runnable alertRunnable = new Runnable() {
        @Override
        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("No Registered Devices Found!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            //  dismissDialog(customerId);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    };

    private final Runnable serverErrorRunnable = new Runnable() {
        @Override
        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Server Error. Please try again later.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            //  dismissDialog(id);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    };

    private final Runnable mRunnable3 = new Runnable() {
        @Override
        public void run() {
            findViewById(R.id.ad_window).setVisibility(View.VISIBLE);
            TextView Text1 = (TextView) findViewById(R.id.ad_window_text);
            Text1.setText(
                    Html.fromHtml(
                            "<a href=\"http://www.microsoft.com/Lumia\">Get 50% off. [Coupon:2454]. Order Now!</a> "));
            Text1.setMovementMethod(LinkMovementMethod.getInstance());

        }
    };
    private final Runnable deviceConnector = new Runnable() {
        @Override
        public void run() {
            if (selectedDeviceIndex != -1) {
//                DiscoveryManager.getInstance().start();

                mTV = getImageDevices().get(selectedDeviceIndex);
                mTV.addListener(deviceListener);
                mTV.connect();

                Log.d("TEST_ROKU_6", selectedDeviceIndex + " CONNECTING DEVICE..." + mTV.getFriendlyName());
            }
        }
    };

    private final Runnable deviceRefresher = new Runnable() {
        @Override
        public void run() {

            List<ConnectableDevice> tempList = getImageDevices();
            ArrayList<String> urls = new ArrayList<String>();
            for (int i = 0; i < tempList.size(); i++)
                urls.add(tempList.get(i).getIpAddress());
            new HttpUtil(urls, onValidationOfUrls).execute();


        }
    };

    private CallBack<ArrayList<Boolean>> onValidationOfUrls = new CallBack<ArrayList<Boolean>>() {
        @Override
        public void execute(ArrayList<Boolean> response) {
            ArrayList<ConnectableDevice> finalList = new ArrayList<ConnectableDevice>();
            list = new ArrayList<ConnectableDevice>();
            for (int i = 0; i < getImageDevices().size(); i++) {
                if (response.size() <= i)
                    break;
                Log.d("TEST_ROKU_7", "RESPONSE ARRAY " + response.get(i).booleanValue());
                if (response.get(i).booleanValue()) {

                    list.add(getImageDevices().get(i));
                    Log.d("TEST_ROKU_7", "Addding Final List ARRAY ");
                }
            }
            SettingsListAdapter adapter = (SettingsListAdapter) listview.getAdapter();
            adapter.updateData(list, serverDevicesList);
            if (deviceListDialog.isShowing()) {
                Log.d("TEST_ROKU_5", "POSTING AGAIN");
                mHandler.postDelayed(deviceRefresher, 5000);
            }
        }
    };

    /**
     * this function calls every seconds to update the current time in the screen
     */
    private final Runnable timeUpdater = new Runnable() {
        @Override
        public void run() {
            SimpleDateFormat sdfDate = new SimpleDateFormat("EEEE, hh:mm a");//dd/MM/yyyy
            String curTime = sdfDate.format(new Date());
            ((TextView) findViewById(R.id.datedegreetext)).setText(curTime);
            new Handler().postDelayed(timeUpdater, 1000);


        }
    };


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (dialogDismissed) {
                dialogDismissed = false;
                return true;
            } else if (findViewById(R.id.frame_epg).getVisibility() == View.VISIBLE) {

                return true;
            }
            serverThread = null;
            Toast.makeText(getApplicationContext(), "activity finished", Toast.LENGTH_LONG).show();
            finish();
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
        }
        return super.dispatchKeyEvent(event);
    }


    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }

    class DateChangeReceiver extends BroadcastReceiver {
        private Date lastCheckedDate;
        public DateChangeReceiver() {
            lastCheckedDate = new Date();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // an Intent broadcast.
            final String action = intent.getAction();
            if(mChannelsList == null)
                return;

            if (action.equals("android.intent.action.TIME_TICK") || action.equals("android.intent.action.TIME_SET")) {
                Date currentDate = new Date();
                Calendar cal1 = Calendar.getInstance();
                cal1.setTime(lastCheckedDate);
                int lastCheckedDay = cal1.get(Calendar.DAY_OF_MONTH);
                Calendar cal2 = Calendar.getInstance();
                cal1.setTime(currentDate);
                int today = cal2.get(Calendar.DAY_OF_MONTH);
                if (lastCheckedDay == today)
                    return;

                lastCheckedDate = currentDate;
                if (mChannelsList.getVisibility() == View.VISIBLE)
                    initView();
            }
        }
    }
}