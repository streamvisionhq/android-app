package com.connectsdk.sampler.communicator;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

/**
 * This Class allows communication between the client and server.
 */
public class ServerCommunication {

    /**
     *
     * @param url
     * @return JSONArray from the api response
     */
    public static JSONArray getJSONfromURL(String url){
        String result = "";
        JSONArray jArray = null;
        InputStream is = null;

        // http post
        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpGet request = new HttpGet();
            URI website = new URI(url);
            request.setURI(website);
            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            // Log.i("tag", temp);
        } catch (Exception e) {
            System.out.println("**CONNECTION PROBLEM  2***");
            return jArray;
        }

        // convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.d("CoBrokeMLS", "Result: "+result);
        } catch (Exception e) {
            Log.e("CoBrokeMLS", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONObject(result).getJSONArray("data");
        } catch (JSONException e) {
            Log.e("CoBrokeMLS", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    /**
     * Gets a JSON object from the specified URL.
     * @param url
     * @return JSON object from the URL or null if it can't be parsed.
     */
    public static JSONObject getJSONObjectFromURL(String url){
        String result = "";
        JSONObject jObj = null;
        InputStream is = null;

        // http post
        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpGet request = new HttpGet();
            URI website = new URI(url);
            request.setURI(website);
            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        } catch (Exception e) {
            System.out.println("**CONNECTION PROBLEM  2***");
        }

        // convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.d("GetResult", result);
        } catch (Exception e) {
            Log.e("CoBrokeMLS", "Error converting result " + e.toString());
        }

        try {
            jObj = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("CoBrokeMLS", "Error parsing data " + e.toString());
        }

        return jObj;
    }
}
