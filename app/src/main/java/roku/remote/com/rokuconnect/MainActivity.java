package roku.remote.com.rokuconnect;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    protected Button testDiscoverButton;
    public List<String> appUrls;
    public static MainActivity instance = null;
    public static String APP_NAME = "StreamVision";

    //DISCOVERY
    private DiscoveryThread discoveryThread;
    private List<String> listItems = new ArrayList<String>();
    private String selectedDeviceUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testDiscoverButton = (Button)findViewById(R.id.test_discovery);
        testDiscoverButton.setOnClickListener(discoverListener);
        instance = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public String getSelectedUrl() {
        return selectedDeviceUrl;
    }

    public  void notifyHandler(Message msg) {
        handler.sendMessage(msg);
    }

    public  Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Results.DEVICES_DISCOVERED:
                    Log.d("ROKU_CONNECT", "Discovered a device with URL : " + (String)msg.obj);
                    listItems.add((String)msg.obj);
                    break;
                default:
                    break;
            }
        }
    };

    private View.OnClickListener discoverListener = new View.OnClickListener() {
        public void onClick(View v) {
            testDiscoverButton.setEnabled(false);
            sendDiscoveryRequest();
        }
    };

    public void sendDiscoveryRequest() {
        appUrls = new java.util.ArrayList<String>();
        discoveryThread = new DiscoveryThread(Actions.DISCOVER_DEVICE);
        discoveryThread.start();
    }



}
