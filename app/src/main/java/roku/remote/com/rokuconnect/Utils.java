package roku.remote.com.rokuconnect;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Vivek on 14/05/15.
 */
public class Utils {

    public final static String LOCATION = "LOCATION:";
    public final static String APPLICATION_URL = "Application-URL";

    public static String getUPnPLocation(String M_SEARCH_Response)
    {
        int start = M_SEARCH_Response.indexOf(LOCATION);
        start += LOCATION.length() + 1;
        String location = M_SEARCH_Response.substring(start);
        int end = location.indexOf('\r', 0);
        location = location.substring(0, end);
        return location;
    }

    public static String sendDeviceDescriptionRequest(String upnpLocation) {
        String appUrl = "";
        try {
            URL obj = new URL(upnpLocation);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            appUrl = getHeader(con, APPLICATION_URL);
            in.close();

            //print result
            System.out.println(response.toString());
        } catch (Exception e) {
        }
        return appUrl;
    }

    public static String getHeader(HttpURLConnection con, String header) {
        java.util.List<String> values = new java.util.ArrayList<String>();
        int idx = (con.getHeaderFieldKey(0) == null) ? 1 : 0;
        while (true) {
            String key = con.getHeaderFieldKey(idx);
            if (key == null)
                break;
            if (header.equalsIgnoreCase(key))
                return con.getHeaderField(idx);
            ++idx;
        }
        return "";
    }

}
