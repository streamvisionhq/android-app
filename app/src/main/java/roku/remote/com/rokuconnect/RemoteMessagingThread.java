package roku.remote.com.rokuconnect;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by rburdick on 2/5/14.
 */
public class RemoteMessagingThread extends Thread {
    private String remoteCommand = Actions.REMOTE_PLAY;
    public final static String RUN_LOCATION = "LOCATION";

    public RemoteMessagingThread(String command) {
        remoteCommand = command;
    }

    public void sendCommand() {
        String appRunLocation = "";
        try {
            URL obj = new URL(MainActivity.getInstance().getSelectedUrl() + "/" +MainActivity.APP_NAME);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "text/plain; charset=\"utf-8\"");

            String urlParameters = "command=" + URLEncoder.encode(remoteCommand, "UTF-8");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            appRunLocation = Utils.getHeader(con, RUN_LOCATION);
            /*Message msg = Message.obtain();
            msg.what = MainActivity.APP_LAUNCH_MESSAGE_ID;
            msg.obj = appRunLocation;
            this.client.handler.sendMessage(msg);*/
            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }



    public void run() {
        sendCommand();
    }
}
