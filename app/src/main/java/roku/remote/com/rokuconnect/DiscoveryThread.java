package roku.remote.com.rokuconnect;

import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by Vivek on 06/05/15.
 */
public class DiscoveryThread extends Thread {

    private int MAX_TRIES = 5;
    private int action = Actions.DISCOVER_DEVICE;


    public DiscoveryThread(int actionToBePerformed) {
        action = actionToBePerformed;
    }

    public void discoverDevice() {
        try {
            int readCount = 0;
            String M_SEARCH = "M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nMAN: \"ssdp:discover\"\r\nMX: seconds to delay response\r\nST: urn:dial-multiscreen-org:service:dial:1\r\nUSER-AGENT: RokuCastClient";
            DatagramSocket clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(1000);
            InetAddress IPAddress = InetAddress.getByName("239.255.255.250");
            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];
            sendData = M_SEARCH.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 1900);
            clientSocket.send(sendPacket);
            String response = "";
            while (readCount < MAX_TRIES) {
                Log.d("ROKU_CONNECT", "TESTING");
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                try {
                    clientSocket.receive(receivePacket);
                    response = new String(receivePacket.getData());
                    String upnpLocation = Utils.getUPnPLocation(response);
                    //Move the following function to this thread class
                    String appUrl = Utils.sendDeviceDescriptionRequest(upnpLocation);
                    Message msg = Message.obtain();
                    msg.what = Results.DEVICES_DISCOVERED;
                    msg.obj = appUrl;
                    MainActivity.getInstance().notifyHandler(msg);
                } catch (SocketTimeoutException ste) {
                }
                readCount++;
            }
            clientSocket.close();
        } catch (Exception e) {
            Log.d("ROKU_CONNECT",e.getMessage());
        }
    }

    public void discoverApp() {
        try {
            String url = MainActivity.getInstance().getSelectedUrl();
            URL obj = new URL(url + "/" + MainActivity.APP_NAME);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            String res = response.toString();
            Log.d("App info response", res);

            Message uiMsg = Message.obtain();
            uiMsg.what = Results.APP_DISCOVERED;
            uiMsg.obj = res;
            MainActivity.getInstance().notifyHandler(uiMsg);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void run() {
        switch (action) {
            case Actions.DISCOVER_DEVICE:
                Log.d("ROKU_CONNECT", "CALLING DISCOVER DEVICE");
                this.discoverDevice();
                break;
            case Actions.DISCOVER_APP:
                this.discoverApp();
                break;
            default:
                break;
        }
    }

}
